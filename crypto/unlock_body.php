<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Crypto;

/**
 * Unencrypts and verifies the signature of the message body.
 *
 * @param array $body             The message body (received in a request)
 * @param array $alias_attributes The alias_attributes set for this server
 * @param array $pair_parameters  The shared settings between two servers
 *
 * @return mixed $unlocked_body     The decrypted and verified message body
 */
function unlock_body(array $body, array $alias_attributes, array $pair_parameters): mixed
{
    $unlocked_body = [];

    // If shared_key is set, then client, server, sign, encrypt,
    // client_public_key and server_public_key are probably set.
    if (!isset($pair_parameters['shared_key'])) {
        return $body;
    }

    // Set the public key based on whether or not the server is the client
    if (isset($body['pair']['client'])
        && $body['pair']['client'] === $alias_attributes['alias']) {
        $public_key = $pair_parameters['server_public_key'];
    } else {
        $public_key = $pair_parameters['client_public_key'];
    }

    if (isset($body['locked'])
        && !empty($body['locked'])
        && $pair_parameters['sign'] === true
        && $pair_parameters['encrypt'] === true) {
        $decrypted = Crypto\decrypt_paseto_message($body['locked'], $pair_parameters['shared_key']);
        $unlocked_body = Crypto\verify_paseto_message($decrypted, $public_key);
    } elseif (isset($body['signed'])
              && !empty($body['signed'])
              && $pair_parameters['sign'] === true
              && $pair_parameters['encrypt'] === false) {
        $unlocked_body = Crypto\verify_paseto_message($body['signed'], $public_key);
    } elseif (isset($body['encrypted'])
              && !empty($body['encrypted'])
              && $pair_parameters['sign'] === false
              && $pair_parameters['encrypt'] === true) {
        $unlocked_body = Crypto\decrypt_paseto_message($body['encrypted'], $pair_parameters['shared_key']);
    } else {
        $unlocked_body = $body;
    }

    return $unlocked_body;
}
