<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\CrudJson;
use AliasAPI\Crypto;

/**
 * Signs and encrypts a body and removes all of the elements except pair
 * Use the pair file to lock & unlock since sign & encrypt are set server side.
 *
 * @return array $locked_body locked | signed | encrypted | unencryted or empty array
 */
function lock_body(array $body, array $alias_attributes, array $pair_parameters): array
{
    $locked_body = [];

    if ((!isset($pair_parameters['shared_key'])
        || empty($pair_parameters['shared_key']))
        || (!isset($alias_attributes['api_keys']['private_key'])
        || empty($alias_attributes['api_keys']['private_key']))) {
        return $body;
    }

    // Only the pair client & server aliases are needed to unlock the body
    $locked_body['pair']['client'] = $pair_parameters['client'];
    $locked_body['pair']['server'] = $pair_parameters['server'];

    // Remember sign and encrypt come from the pair file, AND alias_attributes
    if ($pair_parameters['sign'] === true
        && $pair_parameters['encrypt'] === true) {
        $signed = Crypto\sign_paseto_message(
            $body,
            $alias_attributes['api_keys']['private_key']
        );
        $locked_body['locked'] = Crypto\encrypt_paseto_message(
            $signed,
            $pair_parameters['shared_key']
        );
    } elseif ($pair_parameters['sign'] === true
              && $pair_parameters['encrypt'] === false) {
        $locked_body['signed'] = Crypto\sign_paseto_message(
            $body,
            $alias_attributes['api_keys']['private_key']
        );
    } elseif ($pair_parameters['sign'] === false
              && $pair_parameters['encrypt'] === true) {
        $json_body = CrudJson\encode_json($body, '');
        $locked_body['encrypted'] = Crypto\encrypt_paseto_message(
            $json_body,
            $pair_parameters['shared_key']
        );
    } else {
        $locked_body = $body;
    }

    return $locked_body;
}
