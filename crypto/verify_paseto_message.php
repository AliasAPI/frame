<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricPublicKey;
use ParagonIE\Paseto\Protocol\Version2;

/**
 * Verify the paseto message.
 *
 * @param string $signed_message the signed paseto message
 * @param string $public_key     the client public key
 *
 * @return mixed returns a 403 error if there is a problem with verification
 */
function verify_paseto_message(string $signed_message, string $public_key): mixed
{
    if ($public_key === '') {
        Messages\respond(500, ['The public key needed to verify is not set yet.']);
    }

    try {
        $public_key_binary = Base64UrlSafe::decode($public_key);

        $public_key_object = new AsymmetricPublicKey($public_key_binary);

        $verified_message = Version2::verify($signed_message, $public_key_object);

        return \json_decode($verified_message, true);
    } catch (PasetoException $ex) {
        Messages\respond(403, ['The message has an invalid signature.']);
    }
}
