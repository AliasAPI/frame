<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Keys\Version2\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;

/**
 * Creates an encryption for the shared key.
 *
 * @param array $keys the key to be encrypted
 *
 * @return string returns the shared key
 */
function derive_paseto_shared_key(array $keys): string
{
    // The sequence of keys matters in calculating the resulting shared_key.
    // Sort keys in alphabetical order to reduce human error.
    \ksort($keys);

    // Convert the array to JSON
    $json_keys = \json_encode($keys);

    // md5() encode
    $md5_keys = \md5($json_keys);

    $string_length = \mb_strlen($md5_keys);

    $key_length = Version2::getSymmetricKeyByteLength();

    $excess_character_count = $string_length - $key_length;

    $half_excess_count = (int) \floor($excess_character_count / 2);

    // Get $key_length characters starting from the $half_excess_count character
    $middle_section = \mb_strimwidth($md5_keys, $half_excess_count, $key_length);

    // "Use the middle section of the keys to create a shared symmetric key
    $shared_key_obj = new SymmetricKey($middle_section);

    return Base64UrlSafe::encode($shared_key_obj->raw());
}
