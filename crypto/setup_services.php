<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Crypto;

/**
 * Creates the services configs file.
 *
 * @return array Returns true if the .services.json file is created or exists.
 */
function setup_services(array $request): array
{
    $services = [];

    if ($request['action'] !== 'setup services') {
        return $services;
    }

    $services[$request['pair']['client']] = [
        'alias' => $request['pair']['client'],
        'api_pass' => Crypto\create_random_token(24, false),
        'url' => $request['pair']['client_url'] ?? ''
    ];

    $services[$request['pair']['server']] = [
        'alias' => $request['pair']['server'],
        'api_pass' => Crypto\create_random_token(24, false),
        'url' => $request['pair']['server_url'] ?? ''
    ];

    return $services;
}
