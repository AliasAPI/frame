<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricSecretKey;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\Purpose;

/**
 * Creates a paseto token.
 *
 * @param string $local_private_key   The private key (configured in alias_attributes)
 * @param string $expiration_interval Ex: +1 day, +2 hours, +10 minutes, +3600 seconds
 * @param array  $extra_claims        Any data (or the same parameters for JWT tokens)
 *
 * @return string token                 Returns a pasteo toke or a 400 error
 */
function create_paseto_token(
    string $local_private_key,
    string $expiration_interval,
    array $extra_claims
): string {
    $paseto_token = '';

    try {
        $local_private_key_bin = Base64UrlSafe::decode($local_private_key);
        $local_private_key_obj = new AsymmetricSecretKey($local_private_key_bin);

        // Converts automatically to a string
        $paseto_token_obj = (new Builder())
            // Use the private_key to prove this server set the token
            ->setKey($local_private_key_obj)
            // Only use Version 2
            ->setVersion(new Version2())
            // Public keys are for authentication and authorization
            ->setPurpose(Purpose::public())
            // Set the time that the paseto token will expire
            ->setExpiration(new \DateTimeImmutable($expiration_interval))
            // Store arbitrary data
            ->setClaims($extra_claims);

        $paseto_token = (string) $paseto_token_obj;
    } catch (PasetoException $ex) {
        Messages\respond(400, [$ex->getMessage()]);
    }

    return $paseto_token;
}
