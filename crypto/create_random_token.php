<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

/**
 * Creates a random token.
 *
 * @param int  $length The length of the token
 * @param bool $secure Set to FALSE to create a less insecure string
 *
 * @return string $token     returns the random token
 */
function create_random_token(int $length = 24, bool $secure = true): string
{
    $token = '';

    if ($secure === false) {
        // Define a character set that is easily communicated over the phone
        $alpha = 'AFHILKQRSW';
        $nonce = '2345679';
        $length = ((int) $length < 6) ? 6 : $length;
        $secure = 'XY';
    } else {
        $alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $nonce = '0123456789-=~!@#$%^&*()_+,.?;:[]{}|';
        $length = ((int) $length < 16) ? 16 : $length;
        $secure = \hash('sha256', \bin2hex(\random_bytes(33)), false);
    }

    $keyspace = \array_merge(\mb_str_split($alpha), \mb_str_split($nonce), \mb_str_split($secure));

    \shuffle($keyspace);

    // Count the characters in the keyspace
    $characterslength = \count($keyspace) - 1;

    while (\mb_strlen($token) < $length) {
        $token .= $keyspace[random_int(0, $characterslength)];

        // Make sure the string contains a number
        if (\preg_match('~[0-9]~', $token) !== 1) {
            $token .= \random_int(2, 9);
        }

        // Make sure the string begins with an uppercase
        $token = \preg_replace('/^[^A-Z]+/', '', $token);
    }

    return $token;
}
