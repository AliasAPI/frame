<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Crypto;
use AliasAPI\Messages;

/**
 * Sets the array for the .alias.json file.
 *
 * @param array  $services The file that lists the microservices
 * @param string $side     The clientside or serverside microservice
 *
 * @return array returns array if successful
 */
function setup_alias(array $request, array $services, string $side): array
{
    $alias = '';
    $api_pass = '';
    $alias_array = [];

    if (!isset($request['action'])
        || $request['action'] !== 'setup services') {
        return $alias_array;
    }

    if ($side === 'client') {
        $alias = $request['pair']['client'];
        $api_pass = $services[$alias]['api_pass'];
        // Set the alias and api_pass for the server during the initial setup
        $server = $request['pair']['server'];
        Messages\set_auth($server, $services[$server]['api_pass']);
    } elseif ($side === 'server') {
        $creds = Messages\get_request_credentials();
        $alias = $creds['api_user'];
        $api_pass = $creds['api_pass'];
    } else {
        Messages\respond(400, ['The alias config could not be set.']);
    }

    [$private_key, $public_key] = Crypto\generate_paseto_keypair();

    $salt = Crypto\create_random_token(24, true);

    $alias_array[$alias]['alias'] = $alias;

    $alias_array[$alias]['api_keys'] = [
        'api_pass' => "{$api_pass}",
        'private_key' => "{$private_key}",
        'public_key' => "{$public_key}",
        'salt' => $salt
    ];

    $alias_array[$alias]['authorized_actions'] = [
        'create pair file',
        'read pair file',
        'delete pair file',
        'ping service',
        'setup services'
    ];

    if ($side !== 'client') {
        $alias_array[$alias]['crypto_config'] = [
            'encrypt' => false,
            'sign' => false
        ];

        if (\defined('DATABASE_DSN')
            && getenv('DATABASE_DSN', true)) {
            $dsn = DATABASE_DSN;
        } else {
            $dsn = 'mysql:host=mariadb;port=3306;charset=utf8mb4';
        }

        $alias_array[$alias]['database_config'] = [
            'database' => \mb_strtolower($alias),
            'dsn' => $dsn,
            'username' => 'root',
            'password' => ''
        ];

        $alias_array[$alias]['debug_config'] = [
            'detail_level' => 0,
            'environment' => 'production'
        ];

        $alias_array[$alias]['email_config'] = [
            'api_url' => 'smtp.gmail.com',
            'username' => '',
            'password' => '',
            'email_port' => 465,
            'send_email' => false,
            'sent_url' => '{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail'
        ];

        $alias_array[$alias]['gateway_config'] = [
            'gateway' => 'PayPal_Rest',
            'email' => '',
            'username' => '',
            'password' => '',
            'testmode' => false
        ];

        // Create a random version 4 uuid for the alias
        $alias_array[$alias]['service_uuid'] =
            sprintf(
                '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                random_int(0, 0xFFFF),
                random_int(0, 0xFFFF),
                // 16 bits for "time_mid"
                random_int(0, 0xFFFF),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                random_int(0, 0x0FFF) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                random_int(0, 0x3FFF) | 0x8000,
                // 48 bits for "node"
                random_int(0, 0xFFFF),
                random_int(0, 0xFFFF),
                random_int(0, 0xFFFF)
            );
    }

    $alias_array[$alias]['side'] = $side;

    \ksort($alias_array[$alias]);

    return $alias_array;
}
