<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;

/**
 * Encrypts the paseto message.
 *
 * @param string $message    the message to be encrypted
 * @param string $shared_key the shared key
 *
 * @return string Returns a 400 error if there is a problem with encrpytion. Otherwise returns the encrypted message.
 */
function encrypt_paseto_message(string $message, string $shared_key): string
{
    try {
        $shared_key_binary = Base64UrlSafe::decode($shared_key);

        $shared_key_object = new SymmetricKey($shared_key_binary);

        $encrypted_message = Version2::encrypt($message, $shared_key_object);

        return Base64UrlSafe::encode($encrypted_message);
    } catch (PasetoException $ex) {
        Messages\respond(400, [$ex->getMessage()]);

        exit;
    }
}
