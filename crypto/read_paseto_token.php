<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricPublicKey;
use ParagonIE\Paseto\Parser;
use ParagonIE\Paseto\ProtocolCollection;
use ParagonIE\Paseto\Purpose;
use ParagonIE\Paseto\Rules\NotExpired;

/**
 * Reads a paseto token.
 *
 * @param string $local_private_key The local private key
 * @param string $received_token    The received token
 *
 * @return mixed $token_claims   Returns a 400 error if there is a problem with the token
 */
function read_paseto_token(string $local_private_key, string $received_token): mixed
{
    $token_claims = '';

    try {
        $local_private_key_bin = Base64UrlSafe::decode($local_private_key);

        $local_private_key_obj = new AsymmetricPublicKey($local_private_key_bin);

        $parser = (new Parser())
            ->setPurpose(Purpose::public())
            // Use the server's private key to decode the token
            ->setKey($local_private_key_obj, true)
            // Adding rules to be checked against the token
            ->addRule(new NotExpired())
            // Only allow Version 2 of the Paseto Program
            ->setAllowedVersions(ProtocolCollection::v2());

        $parsed_token_obj = $parser->parse($received_token);

        $token_claims = $parsed_token_obj->getClaims();
    } catch (PasetoException $ex) {
        Messages\respond(400, [$ex->getMessage()]);
    }

    return $token_claims;
}
