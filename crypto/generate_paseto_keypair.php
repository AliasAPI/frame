<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Keys\Version2\AsymmetricPublicKey;
use ParagonIE\Paseto\Keys\Version2\AsymmetricSecretKey;

/**
 * Generates a keypair for the paseto.
 *
 * @return array returns the pair key
 */
function generate_paseto_keypair(): array
{
    $keypair = \sodium_crypto_sign_keypair();

    $private_key_obj = new AsymmetricSecretKey(sodium_crypto_sign_secretkey($keypair));
    $public_key_obj = new AsymmetricPublicKey(sodium_crypto_sign_publickey($keypair));

    // Safe String for Storage
    $private_key = Base64UrlSafe::encode($private_key_obj->raw());
    $public_key = Base64UrlSafe::encode($public_key_obj->raw());

    return [$private_key, $public_key];
}
