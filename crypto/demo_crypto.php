<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

// demo_crypto();

/*
 * Demo file for the crypto function.
 *
 * @return void
 */
// function demo_crypto()
// {
//     require_once(\realpath(\dirname(__FILE__) . '/../../../autoload.php'));
//     require_once('../debug/say.php');

//     require_once('create_paseto_token.php');
//     require_once('read_paseto_token.php');
//     require_once('generate_paseto_keypair.php');
//     require_once('derive_paseto_shared_key.php');
//     require_once('sign_paseto_message.php');
//     require_once('verify_paseto_message.php');
//     require_once('encrypt_paseto_message.php');
//     require_once('decrypt_paseto_message.php');
//     require_once('../construe/convert_timezone.php');
//     require_once('../crudjson/encode_json.php');
//     require_once('../crudjson/decode_json.php');
//     require_once('../crudjson/check_json_error.php');
//     require_once('create_random_token.php');
//     require_once('lock_body.php');
//     require_once('unlock_body.php');

//     echo "CLIENT<br>";
//     echo "generate_paseto_keypair()<br>";
//     list($client_private_key, $client_public_key) = generate_paseto_keypair();
//     echo "client_private_key [ $client_private_key ]<br>";
//     echo "client_public_key [ $client_public_key ]<hr>";

//     echo "SERVER<br>";
//     echo "generate_paseto_keypair()<br>";
//     list($server_private_key, $server_public_key) = generate_paseto_keypair();
//     echo "server_private_key [ $server_private_key ]<br>";
//     echo "server_public_key [ $server_public_key ]<hr>";

//     echo "derive_paseto_shared_key()<br>";
//     $keys = [];
//     $keys['client_public_key'] = $client_public_key;
//     $keys['server_public_key'] = $server_public_key;
//     $shared_key = derive_paseto_shared_key($keys);
//     echo "client shared_key:<br>$shared_key<hr>";

//     // Running this again to demonstrate how both servers can get the same result.
//     $keys_2['server_public_key'] = $server_public_key;
//     $keys_2['client_public_key'] = $client_public_key;
//     $shared_key_2 = derive_paseto_shared_key($keys_2);
//     echo "server shared_key:<br>$shared_key_2<hr>";

//     echo "sign_paseto_message()<br>";
//     $client_message = array('Here' => 'is a', 'test' => 'message', 'in' => 'a signed message');
//     print_r($client_message);
//     echo "<br>";
//     $signed_message = sign_paseto_message($client_message, $client_private_key);
//     echo "signed_message:<br>$signed_message<hr>";

//     echo "encrypt_paseto_message()<br>";
//     $encrypted_message = encrypt_paseto_message($signed_message, $shared_key);
//     echo "encrypted_message:<br>$encrypted_message<hr>";

//     echo "decrypt_paseto_message()<br>";
//     $signed_message = decrypt_paseto_message($encrypted_message, $shared_key);
//     echo "signed_message:<br>$signed_message<hr>";

//     echo "verify_paseto_message()<br>";
//     $verified_message = verify_paseto_message($signed_message, $client_public_key);
//     echo "verified_message:<br>";
//     print_r($verified_message); echo "<hr>";

//     echo "<pre>";

//     $pair_parameters['client'] = 'TestClient';
//     $pair_parameters['server'] = 'TestServer';
//     $pair_parameters['client_public_key'] = $client_public_key;
//     $pair_parameters['server_public_key'] = $server_public_key;
//     $pair_parameters['sign'] = true;
//     $pair_parameters['encrypt'] = true;
//     $pair_parameters['shared_key'] = $shared_key;

//     // lock_body() clientside
//     $body = ['This' => 'is a', 'message' => 'to lock', 'and' => 'unlock'];
//     $alias_attributes['api_keys']['private_key'] = $client_private_key;
//     $locked_body = lock_body($body, $alias_attributes, $pair_parameters);
//     echo "client locked_body:<br>";
//     print_r($locked_body); echo "<hr>";

//     // unlock_body() serverside
//     $alias_attributes = [];
//     $alias_attributes['alias'] = 'TestServer';
//     $alias_attributes['api_keys']['private_key'] = $server_private_key;
//     $unlocked_body = unlock_body($locked_body, $alias_attributes, $pair_parameters);
//     echo "server unlocked_body:<br>";
//     print_r($unlocked_body); echo "<hr>";

//     // lock_body() serverside
//     $body = ['This' => 'is the', 'response' => 'to send', 'back' => 'to the client'];
//     $locked_body = lock_body($body, $alias_attributes, $pair_parameters);
//     echo "server locked_body:<br>";
//     print_r($locked_body); echo "<hr>";

//     // unlock_body() clientside
//     $alias_attributes = [];
//     $alias_attributes['alias'] = 'TestClient';
//     $alias_attributes['api_keys']['private_key'] = $client_private_key;
//     $unlocked_body = unlock_body($locked_body, $alias_attributes, $pair_parameters);
//     echo "client unlocked_body:<br>";
//     print_r($unlocked_body); echo "<hr>";

//     echo "Paseto extra claims:";
//     $expiration_interval = '+1 day';
//     $extra_claims = array('alias' => 'SandboxRest', 'user' => 'UUID', 'cart' => 'promote-1');
//     print_r($extra_claims);
//     echo "<hr>";

//     echo "create_paseto_token()<br>";
//     $paseto_token = create_paseto_token($server_private_key, $expiration_interval, $extra_claims);
//     echo "paseto_token:<br>$paseto_token<hr>";

//     echo "read_paseto_token()<br>";
//     $token_claims = read_paseto_token($server_public_key, $paseto_token);
//     echo "token_claims<br>" . print_r($token_claims, true) . "<hr>";
// }
