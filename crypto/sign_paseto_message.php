<?php

declare(strict_types=1);

namespace AliasAPI\Crypto;

use AliasAPI\CrudJson;
use AliasAPI\Messages;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\Version2\AsymmetricSecretKey;
use ParagonIE\Paseto\Protocol\Version2;

/**
 * Prepares a signed message to be transmitted to a remote server.
 *
 * @param array  $message           The paseto message to be signed
 * @param string $local_private_key The local private key
 *
 * @return string Returns a signed message or 400 error
 */
function sign_paseto_message(array $message, string $local_private_key): string
{
    $signed_message = '';

    $local_private_key_binary = Base64UrlSafe::decode($local_private_key);

    $local_private_key_object = new AsymmetricSecretKey($local_private_key_binary);

    $json_message = CrudJson\encode_json($message, '');

    try {
        $signed_message = Version2::sign($json_message, $local_private_key_object);
    } catch (PasetoException $e) {
        Messages\respond(400, [$e->getMessage()]);
    }

    return $signed_message;
}
