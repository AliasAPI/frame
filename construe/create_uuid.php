<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * Creates a Version 5 UUID using the alias and namespace uuid.
 * Using the microservice alias, namespace uuid, and a unique
 * string facilitate recreating the uuid.
 *
 * @param string $name           The alias of the microservice
 * @param string $namespace_uuid The uuid set in .alias.json
 * @param string $string         The unique username or email
 *
 * @return string $uuid              Returns the uuid based on the inputs
 */
function create_uuid(string $name, string $namespace_uuid, string $string): string
{
    // If the $namespace_uuid input is not valid, return an empty string
    if (!\preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $namespace_uuid)) {
        return '';
    }

    // Get components of the namespace uuid
    $space = \str_replace(['-', '{', '}'], '', \mb_strtolower($namespace_uuid));

    $namespace_hex = '';

    // Convert Namespace UUID to bits (Binary Value)
    for ($i = 0; $i < \mb_strlen($space); $i += 2) {
        $namespace_hex .= \chr(\hexdec($space[$i] . $space[$i + 1]));
    }

    // Calculate hash value of all the inputs
    $hash = \hash('sha256', \mb_strtoupper($name) . \mb_strtolower($namespace_hex) . $string, false);

    // Format the UUID
    $uuid = \sprintf(
        '%08s-%04s-%04x-%04x-%12s',
        // 32 bits for "time_low"
        \mb_substr($hash, 0, 8),
        // 16 bits for "time_mid"
        \mb_substr($hash, 8, 4),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 5
        (\hexdec(\mb_substr($hash, 12, 4)) & 0x0FFF) | 0x5000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (\hexdec(\mb_substr($hash, 16, 4)) & 0x3FFF) | 0x8000,
        // 48 bits for "node"
        \mb_substr($hash, 20, 12)
    );

    return $uuid;
}
