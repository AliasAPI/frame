<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

use AliasAPI\Construe;
use AliasAPI\CrudJson;

/**
 * Sets the train values.
 *
 * @param array $request  the request array
 * @param array $tag_file the tag file array
 *
 * @return array returns the train as an array
 */
function set_train(array $train, array $request, array $tag_file): array
{
    $body = [];

    // If the request body is JSON
    if (isset($request['headers']['Content-Type'][0])
        && \mb_strpos($request['headers']['Content-Type'][0], 'application/json') === 0
        && isset($request['body'])
        && !empty($request['body'])) {
        $json = $request['body'];

        $body = CrudJson\decode_json($json);
    }

    $train['settings']['method'] = $request['method'] ?? '';
    $train['settings']['http_version'] = $request['http_version'] ?? '';

    // Condense the $request with array_merge_recursive()
    return Construe\merge_inputs(
        $train,
        $request['params'],
        $body,
        $tag_file
    );
}
