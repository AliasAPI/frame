<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * Parse a query string with duplicate fields in the CGI standard way.
 *
 * @param string $string the parameter to be parsed
 *
 * @return array returns the parsed parameters as an array
 */
function parse_params(string $string): array
{
    $i = 0;
    $params = [];

    if (\mb_strpos($string, '&') === true) {
        // Split on the outer delimiter
        $pairs = \explode('&', $string);

        // Loop through each pair
        foreach ($pairs as $i) {
            // split into name and value
            [$name, $value] = \explode('=', $i, 2);

            // If the index name already exists
            if (isset($params[$name])) {
                // Add multiple values into the array
                if (\is_array($params[$name])) {
                    $params[$name][] = $value;
                } else {
                    $params[$name] = [$params[$name], $value];
                }
            }
            // Otherwise, simply add it as a scalar
            else {
                $params[$name] = $value;
            }
        }
    } elseif (\mb_strpos($string, '=') === true) {
        [$name, $value] = \explode('=', $string, 2);
        $params[$name] = $value;
    }

    return $params;
}
