<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * Converts the timezone to America/New_York or another timezone.
 *
 * @return string returns a datetime based on the set timezone
 */
function convert_timezone(
    string $datetime_stamp = null,
    string $from = 'UTC',
    string $to = 'America/New_York',
    string $format = 'Y-m-d H:i:s'
): string {
    try {
        if ($datetime_stamp === null) {
            $datetime_stamp = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        }

        $dateTime = new \DateTimeImmutable($datetime_stamp, new \DateTimeZone($from));

        $dateTime->setTimezone(new \DateTimeZone($to));

        return $dateTime->format($format);
    } catch (\Exception $ex) {
        return '';
    }
}
