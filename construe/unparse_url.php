<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

use AliasAPI\Construe;

/**
 * Combines a parsed URL into a web address (string).
 *
 * @param array $parsed_url the key value pairs used to create the URL
 * @param array $changes    the key value pairs to replace in the URL
 *
 * @return string returns the URL as a string
 */
function unparse_url(array $parsed_url, array $changes): string
{
    // If the query string is added after Construe\parse_url()
    if ((!isset($parsed_url['params'])
        || empty($parsed_url['params']))
        && (isset($parsed_url['query']))
        && !empty($parsed_url['query'])) {
        // Parse a query string with duplicate fields in the CGI standard way
        $parsed_url['params'] = Construe\parse_params($parsed_url['query']);
    }

    $parsed_url = Construe\merge_inputs($parsed_url, $changes);

    $scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host = $parsed_url['host'] ?? '';
    $port = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user = $parsed_url['user'] ?? '';
    $pass = $parsed_url['pass'] ?? '';
    $cred = (!empty($user) && !empty($pass)) ? "{$user}:{$pass}@" : '';
    $dirs = $parsed_url['dirs'] ?? '';
    $path = $parsed_url['path'] ?? '';
    $file = $parsed_url['file'] ?? '';
    $params = isset($parsed_url['params']) ? '?' . \http_build_query($parsed_url['params']) : '';
    $params = \html_entity_decode($params);
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

    return \htmlspecialchars_decode("{$scheme}{$cred}{$host}{$port}{$dirs}{$file}{$params}{$fragment}");
}
