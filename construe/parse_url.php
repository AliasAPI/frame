<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

use AliasAPI\Construe;

/**
 * Parse a URL with duplicate fields in the CGI standard way.
 *
 * @param string $url the web address (URL) to be checked
 *
 * @return array returns the URL as key value pairs
 */
function parse_url($url): array
{
    if (!\filter_var($url, FILTER_VALIDATE_URL)) {
        return [];
    }

    $parts = [];
    $parts['scheme'] = \parse_url($url, PHP_URL_SCHEME);
    $parts['user'] = \parse_url($url, PHP_URL_USER);
    $parts['pass'] = \parse_url($url, PHP_URL_PASS);
    $parts['host'] = \parse_url($url, PHP_URL_HOST);
    $parts['port'] = \parse_url($url, PHP_URL_PORT);
    $parts['path'] = \parse_url($url, PHP_URL_PATH);

    if (!empty(\pathinfo(\parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION))) {
        $parts['file'] = \pathinfo(\parse_url($url, PHP_URL_PATH), PATHINFO_BASENAME);
        $parts['dirs'] = \str_replace($parts['file'], '', $parts['path']);
    } else {
        $parts['file'] = '';
        $parts['dirs'] = \parse_url($url, PHP_URL_PATH);
    }

    $parts['query'] = \parse_url($url, PHP_URL_QUERY);

    if (!empty($parts['query'])) {
        // Parse a query string with duplicate fields in the CGI standard way
        $parts['params'] = Construe\parse_params($parts['query']);
    }

    $parts['fragment'] = \parse_url($url, PHP_URL_FRAGMENT);

    return $parts;
}
