<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * This will build the URL of the server insecurely.
 *
 * @return string returns the web address as a string
 */
function get_script_url(): string
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $url = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    } else {
        $url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    }

    return $url;
}
