<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * Merges the inputs into an array.
 *
 * @return array returns the merged inputs as an array
 */
function merge_inputs(): array
{
    $merged = [];
    $inputs = func_get_args();

    // Loop through all of the array inputs
    foreach ($inputs as $input) {
        // If an object is passed in . . .
        if (\is_object($input)) {
            // . . . convert it to an array
            $input = (array) $input;
        }

        // If the input is (now) an array . . .
        if (\is_array($input) && !empty($input)) {
            // . . . merge it recursively
            if (empty($merged)) {
                $merged = \array_merge_recursive([], $input);
            } else {
                $merged = \array_replace_recursive($merged, $input);
            }
        // If a scalar is input . . .
        } elseif (!empty($input)) {
            // . . . just add it as an element
            $merged[] = $input;
        }
    }

    // Sort the array alphabetically by key to make debugging easier
    \ksort($merged);

    return $merged;
}
