<?php

declare(strict_types=1);

namespace AliasAPI\Construe;

/**
 * Decodes base64 format into string.
 *
 * @param string $string the base64 string
 *
 * @return string returns the decoded string
 */
function decode_base64(string $string): string
{
    // If there are only valid characters
    if (!\preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) {
        // If the string is strict base64 alphabet
        if (\base64_decode($string, true)) {
            $decoded_string = \base64_decode($string, true);
            // If the re-encoded string matches exactly
            if (\base64_encode($decoded_string) === $string) {
                return $decoded_string;
            }
        }
    }

    return $string;
}
