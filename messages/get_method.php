<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

// Note: GuzzleHttp Client accepts $auth = [];
// Only send the auth parameters when pairing

/**
 * Gets the http method for the message.
 *
 * @return string $method   returns the http method of the message if true
 */
function get_method(): string
{
    if (!\array_key_exists('only_set_in_messages_set_method', $GLOBALS)) {
        Messages\respond(501, ['The request method is not set.']);
    }

    return $GLOBALS['only_set_in_messages_set_method'];
}
