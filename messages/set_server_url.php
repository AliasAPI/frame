<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Sets the Server URL.
 *
 * @param string $server_url the server URL
 *
 * @return mixed returns the server URL for the message
 */
function set_server_url(string $server_url): mixed
{
    $server_url = \filter_var($server_url, FILTER_SANITIZE_URL);

    $server_url = \filter_var(
        $server_url,
        FILTER_VALIDATE_URL
    );

    if (!$server_url) {
        Messages\respond(500, ['The server_url is not good enough.']);
    }

    $GLOBALS['only_set_in_messages_set_server_url'] = $server_url;

    return $GLOBALS['only_set_in_messages_set_server_url'];
}
