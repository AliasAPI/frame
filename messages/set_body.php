<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * Sets the message body.
 *
 * @param array $body The body of the request
 *
 * @return string $GLOBALS['only_set_in_messages_set_body']
 *                Returns the message body
 */
function set_body(array $body): string
{
    if (\count($body) === 0) {
        Messages\respond(501, ['The message body is not set correctly.']);
    }

    $json_body = CrudJson\encode_json($body, '');

    $GLOBALS['only_set_in_messages_set_body'] = $json_body;

    return $GLOBALS['only_set_in_messages_set_body'];
}
