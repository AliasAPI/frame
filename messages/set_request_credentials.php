<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Decodes the request credentials of the message request.
 *
 * @param array $request the request message
 *
 * @return array returns the credentials as an array
 */
function set_request_credentials(array $request): array
{
    $GLOBALS['only_set_in_messages_set_request_credentials'] = [];

    if (isset($request['headers']['Authorization'][0])) {
        $basic_header = $request['headers']['Authorization'][0];

        // Remove the Basic and space from the auth header
        $creds = \str_replace('Basic ', '', $basic_header);

        // Decode the encoding by Guzzle
        $creds = \base64_decode($creds, true);

        // Split the username from the password
        $creds_array = \explode(':', $creds);

        $credentials = [];

        $credentials['api_user'] = $creds_array[0];

        // Decode the password encoding by Messages\set_variable_options();
        $credentials['api_pass'] = \base64_decode($creds_array[1], true);

        $GLOBALS['only_set_in_messages_set_request_credentials'] = $credentials;
    }

    return $GLOBALS['only_set_in_messages_set_request_credentials'];
}
