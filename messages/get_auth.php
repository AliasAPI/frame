<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Gets the authoritization credentials set in set_auth()
 * Note: GuzzleHttp Client accepts $auth = [];
 * Only send the auth parameters when pairing.
 *
 * @return array Returns the message auth credentials
 */
function get_auth(): array
{
    if (\array_key_exists('only_set_in_messages_set_auth', $GLOBALS)) {
        return $GLOBALS['only_set_in_messages_set_auth'];
    }

    return [];
}
