<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Gets the request tag set in set_request_tag().
 *
 * @return string returns the request tag
 */
function get_request_tag(): ?string
{
    if (!\array_key_exists('only_set_in_messages_set_request_tag', $GLOBALS)) {
        return '';
    }

    return $GLOBALS['only_set_in_messages_set_request_tag'];
}
