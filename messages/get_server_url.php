<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Gets the server URL set in the global config.
 *
 * @return null|string returns the URL
 */
function get_server_url(): ?string
{
    if (!\array_key_exists('only_set_in_messages_set_server_url', $GLOBALS)) {
        Messages\respond(501, ['The server url is not set yet.']);
    }

    return $GLOBALS['only_set_in_messages_set_server_url'];
}
