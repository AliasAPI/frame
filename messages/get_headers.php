<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Gets the headers for the the message.
 *
 * @return array Returns the headers for the message if they have been set
 */
function get_headers(): array
{
    if (\array_key_exists('only_set_in_messages_set_headers', $GLOBALS)) {
        return $GLOBALS['only_set_in_messages_set_headers'];
    }

    return [];
}
