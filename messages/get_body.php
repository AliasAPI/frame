<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Gets the request body of the message from $GLOBALS.
 *
 * @return string returns the body array if set
 */
function get_body(): string
{
    if (!\array_key_exists('only_set_in_messages_set_body', $GLOBALS)) {
        return '';
    }

    return $GLOBALS['only_set_in_messages_set_body'];
}
