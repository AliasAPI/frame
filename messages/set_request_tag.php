<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Sets the request tag for the message.
 *
 * @param array $request The request
 * @param bool  $create  Set $create = true to force new tag creation
 *
 * @return string Returns the tag for the message.
 *                Sends a 403 response if request tag was not set correctly.
 */
function set_request_tag(array $request, bool $create = false): string
{
    $tag = '';

    // Re-use the request tag from the received request by default
    if (isset($request['headers']['Request-Tag'][0])
        && $request['headers']['Request-Tag'][0] !== '') {
        $tag = $request['headers']['Request-Tag'][0];
    } elseif (isset($request['params']['tag'])
        && $request['params']['tag'] !== '') {
        $tag = $request['params']['tag'];
    } elseif (\array_key_exists('only_set_in_messages_set_request_tag', $GLOBALS)
              && $GLOBALS['only_set_in_messages_set_request_tag'] !== '') {
        $tag = $GLOBALS['only_set_in_messages_set_request_tag'];
    }

    if ($tag === ''
        || $create === true) {
        $rand = \mb_substr(\md5(\microtime()), \random_int(0, 26), 5);

        $datetime = (new \DateTimeImmutable())->format('YmdHis');

        $tag = \mb_strtoupper('TAG' . $datetime . '_' . $rand);
    }

    $GLOBALS['only_set_in_messages_set_request_tag'] = $tag;

    return $GLOBALS['only_set_in_messages_set_request_tag'];
}
