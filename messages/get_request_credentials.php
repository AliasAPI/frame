<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Returns the request credentials of the message request.
 *
 * @return array Returns the credentials as an array
 */
function get_request_credentials(): array
{
    if (!\array_key_exists('only_set_in_messages_set_request_credentials', $GLOBALS)) {
        Messages\respond(403, ['The request credentials are not set.']);
    }

    return $GLOBALS['only_set_in_messages_set_request_credentials'];
}
