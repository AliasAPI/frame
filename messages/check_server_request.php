<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Checks if the message request has errors.
 *
 * @param array $request the message request array
 *
 * @return void returns a response(501,500,406) if there is an error
 */
function check_server_request(array $request): void
{
    if (empty($request)) {
        Messages\respond(501, ['The server request is empty.']);
    }

    if (!isset($request['body'])
        || !is_string($request['body'])) {
        Messages\respond(500, ['The request body is broken.']);
    }

    if (!isset($request['params'])
        || !is_array($request['params'])) {
        Messages\respond(500, ['The query params are impaired.']);
    }

    if (empty($request['body'])
        && empty($request['params'])) {
        Messages\respond(406, ['There is no request in the body or params.']);
    }

    if (!isset($request['headers']['Request-Tag'][0])
        && !isset($request['params']['tag'])) {
        Messages\respond(406, ['The request tag is not set correctly.']);
    }
}
