<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Alias;
use AliasAPI\CrudJson;
use AliasAPI\CrudPair;
use AliasAPI\Crypto;
use AliasAPI\Messages;

/**
 * Compiles and sends the http request.
 *
 * @return array returns the response
 */
function request(): array
{
    // IMPORTANT:
    $client = new \GuzzleHttp\Client(['http_errors' => false]);

    $response = $client->request(
        Messages\get_method(),
        // Passing in the full URL here overwrites base_uri
        Messages\get_server_url(),
        [
            'headers' => Messages\get_headers(),
            'auth' => Messages\get_auth(),
            'body' => Messages\get_body()
        ]
    );

    $headers = $response->getHeaders();

    if (isset($headers['Content-Type'][0])
        && \mb_strpos($headers['Content-Type'][0], 'application/json') !== false) {
        // Decode the JSON response as an array
        $decoded_body = CrudJson\decode_json((string) $response->getBody());

        $pair_parameters = CrudPair\get_pair_parameters();

        $alias_attributes = Alias\get_alias_attributes();

        $response_body = Crypto\unlock_body($decoded_body, $alias_attributes, $pair_parameters);
    }

    // If the message is emitted and does not come back as JSON
    if (!isset($response_body)
        || empty($response_body)) {
        $response_body = (string) $response->getBody();
    }

    // If the request was sent to an AliasAPI service, check the Request-Tag
    if (isset($response_body['pairs'])
        && (!isset($headers['Request-Tag'][0])
        || $headers['Request-Tag'][0] !== Messages\get_request_tag())) {
        Messages\respond(500, ['The request and response tags do not match.']);
    }

    return [
        'status_code' => $response->getStatusCode(),
        'reason' => $response->getReasonPhrase(),
        'tag' => Messages\get_request_tag(),
        'body' => $response_body
    ];
}
