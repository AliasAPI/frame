<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Validate the URL and redirect the user's browser.
 *
 * @param string $redirect_url The url where the user will be sent
 */
function redirect_browser(string $redirect_url): void
{
    if ($redirect_url) {
        $url = \htmlspecialchars_decode($redirect_url);

        if (!\filter_var($url, FILTER_VALIDATE_URL)) {
            Messages\respond(500, ["The [ {$url} ] redirect URL is not valid."]);
        }

        if (!\headers_sent()) {
            \header('Location: ' . $url);

            exit;
        }
        echo '<!DOCTYPE html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="UTF-8" />';
        echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
        echo "<title>Redirecting to {$url}</title>";
        echo '</head>';
        echo '<body>';
        echo "Please <a href=\"{$url}\">click here</a> to continue.";
        echo '</body>';
        echo '</html>';

        exit;
    }
}
