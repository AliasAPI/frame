<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * @param int   $status_code The http status code associated with the reply
 * @param array $reply       The reply message to be returned with respond()
 *
 * @return array $GLOBALS['only_set_in_messages_set_reply']
 */
function set_reply(int $status_code, array $reply): array
{
    if (!\array_key_exists('only_set_in_messages_set_reply', $GLOBALS)) {
        $GLOBALS['only_set_in_messages_set_reply'] = [];
    }

    if (!\array_key_exists($status_code, $GLOBALS['only_set_in_messages_set_reply'])) {
        $GLOBALS['only_set_in_messages_set_reply'][$status_code] = [];
    }

    if (\count($reply) === 1) {
        $GLOBALS['only_set_in_messages_set_reply'][$status_code][] = $reply[0];
    } else {
        $GLOBALS['only_set_in_messages_set_reply'][$status_code][] = $reply;
    }

    return $GLOBALS['only_set_in_messages_set_reply'];
}
