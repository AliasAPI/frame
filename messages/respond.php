<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Alias;
use AliasAPI\CrudPair;
use AliasAPI\Crypto;
use AliasAPI\Debug;
use AliasAPI\Messages;
use GuzzleHttp\Psr7\Response;

/**
 * Creates a response message that is returned to the AliasAPI client.
 *
 * @param int   $status_code the status code of the message
 * @param array $body        the message
 *
 * @return void returns the response message
 */
function respond(int $status_code = 200, array $body = []): void
{
    $status_code = Messages\set_status_code($status_code);

    $pair_parameters = CrudPair\get_pair_parameters();

    $alias_attributes = Alias\get_alias_attributes();

    $locked_body = Crypto\lock_body($body, $alias_attributes, $pair_parameters);

    $json_body = Messages\set_body($locked_body);

    $headers = Messages\set_headers([], $json_body);

    $response = new Response(
        $status_code,
        $headers,
        $json_body,
        '1.1',
        null
    );

    $statusLine = sprintf(
        'HTTP/%s %s %s',
        $response->getProtocolVersion(),
        $response->getStatusCode(),
        $response->getReasonPhrase()
    );

    // Emit the HTTP/1.1 200 OK
    \header($statusLine, true);

    foreach ($response->getHeaders() as $name => $values) {
        $responseHeader = \sprintf(
            '%s: %s',
            $name,
            $response->getHeaderLine($name)
        );

        // Emit the Request-Tag header
        \header($responseHeader, false);
    }

    // Emit the JSON response
    echo (string) $response->getBody();

    if (\is_callable('AliasAPI\Debug\log')) {
        $log = [];
        $log['tag'] = Messages\get_request_tag();
        $log['status_code'] = $response->getStatusCode();
        $log['body'] = $body;
        Debug\log($log, 0);
        Debug\write_log(false);
    }

    exit;
}
