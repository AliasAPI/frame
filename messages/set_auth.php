<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Sets the authentication for the message.
 *
 * @param string $username the username
 * @param string $password the password
 *
 * @return array returns the authentication for the message
 */
function set_auth(string $username, string $password): array
{
    // Note: The GuzzleHttp Client accepts $auth = [];
    $GLOBALS['only_set_in_messages_set_auth'] = [];

    if ($username === '') {
        Messages\respond(501, ['The username for the request is not set.']);
    }

    if ($password === '') {
        Messages\respond(501, ['The password for the request is not set.']);
    }

    $GLOBALS['only_set_in_messages_set_auth'] = [
        $username,
        \base64_encode($password)
    ];

    return $GLOBALS['only_set_in_messages_set_auth'];
}
