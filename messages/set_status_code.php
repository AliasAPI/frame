<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Sets the status code of the response.
 *
 * @param int $status_code the http status code
 *
 * @return int Returns the set http status code
 */
function set_status_code(int $status_code): int
{
    $GLOBALS['only_set_in_messages_set_status_code'] = $status_code;

    return $GLOBALS['only_set_in_messages_set_status_code'];
}
