<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Returns the status code for the message.
 *
 * @return mixed Returns the set status code
 */
function get_status_code(): mixed
{
    if (!\array_key_exists('only_set_in_messages_set_status_code', $GLOBALS)) {
        Messages\respond(501, ['The status code is not set yet.']);
    }

    return $GLOBALS['only_set_in_messages_set_status_code'];
}
