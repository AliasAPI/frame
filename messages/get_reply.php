<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

/**
 * Gets the replies based on their respective http status codes
 * Example: get_reply(400, 600, 100); gets all of the replies that are errors.
 * Example: get_reply(0, 399); gets all of the replies that are NOT errors.
 *
 * @param int $minimum_status_code 0 includes all of the good and bad replies
 * @param int $maximum_status_code 599 is the maximum http status code in use
 * @param int $limit               The number of messages to be returned
 *
 * @return array $GLOBALS['only_set_in_messages_set_response']
 */
function get_reply(int $minimum_status_code = 0, int $maximum_status_code = 600, int $limit = 10): array
{
    $i = 0;
    $replies = [];

    if (\array_key_exists('only_set_in_messages_set_reply', $GLOBALS)) {
        // Sort the responses order by status_code descending
        // so 500 errors are debugged before 400 errors show
        // and 400 errors are resolved before 200s are shown
        \krsort($GLOBALS['only_set_in_messages_set_reply']);

        foreach ($GLOBALS['only_set_in_messages_set_reply'] as $status_code => $messages) {
            foreach ($messages as $index => $message) {
                ++$i;

                // Include the range of reply messages
                if ($status_code >= $minimum_status_code
                    && $status_code <= $maximum_status_code) {
                    $replies[$status_code][] = $message;
                }

                if ($i >= $limit) {
                    return $replies;
                }
            }
        }

        return $replies;
    }

    return $replies;
}
