<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use GuzzleHttp\Psr7\ServerRequest;

// method, headers, version, params, cookie, body
/**
 * Gets the server request.
 *
 * @return array Returns the method, headers, version, params, cookie, body
 */
function get_server_request(): array
{
    $request = ServerRequest::fromGlobals();

    $headers = $request->getHeaders();
    $method = $request->getMethod();
    $cookie = $request->getCookieParams();
    // $server_params = (array) $request->getServerParams();
    $version = $request->getProtocolVersion();
    $params = $request->getQueryParams();
    $body = (string) $request->getBody();

    return [
        'headers' => $headers,
        // 'credentials' => $credentials,
        'method' => $method,
        'http_version' => $version,
        'cookie' => $cookie,
        'params' => $params,
        // 'server_params' => $server_params,
        // 'uri' => $uri,
        'body' => $body
    ];
}
