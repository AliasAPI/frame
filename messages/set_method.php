<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Sets the http method of the message.
 *
 * @param string $method the http method to use when sending a request
 *
 * @return string $method returns the method if valid for the message
 */
function set_method(string $method = ''): string
{
    if ($method !== '') {
        $method = \mb_strtoupper($method);
    } else {
        $method = 'POST';
    }

    $methods_list = ['GET', 'POST', 'PUT', 'HEAD', 'DELETE', 'PATCH', 'OPTIONS'];

    if (!\in_array($method, $methods_list, true)) {
        Messages\respond(500, ["Invalid method [{$method}] input for request."]);
    }

    $GLOBALS['only_set_in_messages_set_method'] = $method;

    return $GLOBALS['only_set_in_messages_set_method'];
}
