<?php

declare(strict_types=1);

namespace AliasAPI\Messages;

use AliasAPI\Messages;

/**
 * Sets the header for the message.
 *
 * @param array  $headers the headers for the message
 * @param string $message the message
 *
 * @return array returns the headers
 */
function set_headers(array $headers = [], string $message = ''): array
{
    $default_headers = [];

    // if (\headers_sent()) {
    //     throw new \RuntimeException('Headers were already sent. Aborting.');
    // }

    // $default_headers['Connection'] = 'Keep Alive';
    $default_headers['Content-Type'] = 'application/json';
    $default_headers['Content-Length'] = \mb_strlen($message);
    $default_headers['Request-Tag'] = Messages\get_request_tag();

    if (count($headers) > 0) {
        $headers = \array_replace_recursive($default_headers, $headers);
    } else {
        $headers = $default_headers;
    }

    $GLOBALS['only_set_in_messages_set_headers'] = $headers;

    return $GLOBALS['only_set_in_messages_set_headers'];
}
