<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

/**
 * Determine wether to save the log file or not.
 *
 * @param bool $append Set to TRUE if you want to save the file. Otherwise set to false.
 *
 * @return void [return description]
 */
function write_log(bool $append = true): void
{
    if (\defined('WRITABLEPATH')) {
        $log_file = WRITABLEPATH . '/debug_log.txt';

        // Delete / clear the log file
        if (\file_exists($log_file) && $append === false) {
            \unlink($log_file);
        }

        $log = \print_r($GLOBALS['only_set_in_debug_log'], true);

        $tmp = \file_put_contents($log_file, $log . PHP_EOL, FILE_APPEND);
    }
}
