<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

/**
 * Finds the system user name.
 *
 * @return string Returns the name of the system user
 */
function find_whoami(): ?string
{
    if (isset(\posix_getpwuid(\posix_geteuid())['name'])) {
        $whoami = \posix_getpwuid(\posix_geteuid())['name'];
    } else {
        $whoami = '';
    }

    return $whoami;
}
