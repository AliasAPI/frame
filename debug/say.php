<?php

declare(strict_types=1);

/*
 * say() is a utility function for inspecting strings, variables, and arrays.
 * @usage
 * To echo a string, object, or array, use: say($input);
 * To echo a clean looking backtrace, simply use: say();
 * To echo a say(); and die();, use sayd() the same way.
 * You can be all like say('$var_name', $var, 'tag_it');
 * Backtraces are saved in php_errors.log AND say.log.
 * @param mixed $input The input to inspect.
 * @param string $tag An optional display tag.
 * @return void
 * @author Drew Brown drew.brownjr@gmail.com
 */

if (!function_exists('say')) {
    function say(...$args): void
    {
        global $display, $say_count, $args;
        // 2DO . . . Return FALSE if not in a development environment
        // Increment a count to differentiate the say() outputs.
        ++$say_count;

        // Generate a backtrace with the path, class, file, function, arguments, and objects.
        [$backlog, $pretty, $called_from] = back_trace();

        // Add the backtrace to say.log AND php_errors.log
        say_log($backlog, $pretty);

        // If sayd(); did not provide any arguments, get them
        $arguments = (!$args) ? func_get_args() : $args;

        // Important: Append the display each time for more output
        $display .= display($arguments, $say_count, $called_from);

        if (!defined('SAYS')
            && isset($_SERVER['REQUEST_METHOD'])
            && $_SERVER['REQUEST_METHOD'] === 'GET') {
            echo '<pre>';
            echo $display;
            echo '</pre>';
        }
    }
}

if (!function_exists('sayd')) {
    function sayd(...$args): void
    {
        // This is a say() followed by a die()
        global $args;

        if (!defined('SAYD')) {
            define('SAYD', true);
        }

        // Use sayd($arguments); to output say(); and die();
        $args = func_get_args();

        say();

        exit;
    }
}

if (!function_exists('says')) {
    function says(...$args): void
    {
        // This is the silent version of say()
        // It writes to the logs, but does not display.
        // TIP: To silence ALL of the say() outputs at once,
        // simply add a says() before all other say() calls.
        global $args;

        if (!defined('SAYD')) {
            define('SAYD', true);
        }

        if (!defined('SAYS')) {
            define('SAYS', true);
        }

        // Use sayd($arguments); to output say(); and die();
        $args = func_get_args();

        say();
    }
}

if (!function_exists('display')) {
    function display(array $arguments, int $say_count, string $called_from): string
    {
        $call = "\n\r";
        // $call .= (defined('SAYD')) ? "sayd($say_count)" : "say($say_count)";

        if (defined('SAYD')) {
            $call .= "sayd({$say_count})";
        } elseif (defined('SAYS')) {
            $call .= "says({$say_count})";
        } else {
            $call .= "say({$say_count})";
        }

        // Display Header
        $header = $call . ': ' . count($arguments) . ' arguments | ' . $called_from;

        $body = "\n\r";
        // Display Body
        if ($arguments) {
            foreach ($arguments as $arg) {
                switch (gettype($arg)) {
                    case 'boolean':
                    case 'integer':
                        $body .= gettype($arg) . ': ' . json_encode($arg);

                        break;

                    case 'string':
                        $body .= gettype($arg) . ": {$arg}";

                        break;

                    case 'array':
                        $body .= gettype($arg) . ': ' . print_r($arg, true);

                        break;

                    default:
                        $body .= "\n\r" . gettype($arg) . ': ';
                        ob_start();
                        print_r($arg);
                        $body .= ob_get_clean();

                        if (ob_get_length()) {
                            ob_end_clean();
                        }
                }

                $body .= "\n\r";
            }
        } else {
            if (defined('SAY_LOG')) {
                $pretty_backtrace = file_get_contents(SAY_LOG);
                $body = '<pre>' . $pretty_backtrace . '</pre>';
            }
        }

        // Display Footer
        $footer = (defined('SAYD')) ? '#jumptag die()' : '#jumptag';

        return $header . $body . $footer;
    }
}

if (!function_exists('back_trace')) {
    function back_trace(): array
    {
        $i = 0;
        $backlog = [];
        $function = '';
        $pretty = [];
        $trace_count = 0;

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT);

        foreach ($backtrace as $trace) {
            ++$i;

            if (isset($trace['file']) && basename($trace['file']) !== 'say.php') {
                if (isset($trace['function'])) {
                    $function = $trace['function'] . '()';
                } else {
                    unset($function);
                }

                $backlog[$i]['path'] = $trace['file'];
                $pretty[$i]['path'] = $trace['file'];

                if (isset($trace['class']) && !empty($trace['class']) && isset($function)) {
                    $backlog[$i]['class_function'] = $trace['class'] . '->' . $function;
                    $pretty[$i]['class_function'] = $trace['class'] . '->' . $function;
                } elseif (isset($function)) {
                    $backlog[$i]['class_function'] = $function;
                    $pretty[$i]['class_function'] = $function;
                }

                if (isset($trace['line'])) {
                    $backlog[$i]['line'] = $trace['line'];
                    $pretty[$i]['line'] = $trace['line'];
                }

                if (isset($trace['args']) && !empty($trace['args'])) {
                    $backlog[$i]['args'] = print_r($trace['args'], true);
                    $pretty[$i]['args'] = print_r($trace['args'], true);
                }

                if (isset($trace['object']) && !empty($trace['object'])) {
                    $backlog[$i]['object'] = json_encode($trace['object']);
                    $pretty[$i]['object'] = formatJSON($backlog[$i]['object']);
                }
            }
        }

        // Arrange the backtrace in chronological order
        $backlog = array_reverse($backlog);

        $pretty = array_reverse($pretty);

        $trace_count = count($backlog) - 1;

        date_default_timezone_set('America/New_York');

        $datetime = date('Y-m-d H:i:s');

        $pretty_trace_count_line = $pretty[$trace_count]['line'] ?? '';

        $called_from =
            $pretty[$trace_count]['path'] . ' | line ' . $pretty_trace_count_line . ' | ' . $datetime;

        return [$backlog, $pretty, $called_from];
    }
}

if (!function_exists('say_log')) {
    function say_log(array $backlog, array $pretty): void
    {
        // WRITABLEPATH is set in the code (of the microservice )
        // Check to see if a log path has been set.
        // If not, set it to the location of say.php
        // defined( 'SAY_LOG' ) or define( 'SAY_LOG', __DIR__ . '/say.log' );
        date_default_timezone_set('America/New_York');

        $datetime = date('Y-m-d H:i:s');

        if (\defined('WRITABLEPATH') && !\defined('SAY_LOG')) {
            \defined('SAY_LOG') || \define('SAY_LOG', WRITABLEPATH . '/say_log.txt');
        }

        // Delete the say.log before each run to make it easy to read.
        if (defined('SAY_LOG') && file_exists(SAY_LOG)) {
            unlink(SAY_LOG);
        }

        foreach ($pretty as $index => $trace) {
            $log = $index . '. ';

            if (isset($pretty[$index]['path'])) {
                $log .= $pretty[$index]['path'] . ' | line: ' . $pretty[$index]['line'] . " | {$datetime} " . PHP_EOL;
            }

            $object = (isset($pretty[$index]['object']) && !empty($pretty[$index]['object'])) ? $pretty[$index]['object'] : '';

            if (isset($pretty[$index]['args'])) {
                $log .= $pretty[$index]['class_function'] . PHP_EOL . $pretty[$index]['args'] . $object . PHP_EOL . PHP_EOL;
            }

            // Add the output to the php_errors.php log
            error_log($log);

            if (defined('SAY_LOG')) {
                // Add another copy to a say error log
                // file_put_contents(SAY_LOG, "$log", FILE_APPEND);
                file_put_contents(SAY_LOG, "{$log}");
            }
        }
    }
}

if (!function_exists('formatJSON')) {
    function formatJSON(string $json): mixed
    {
        $tab = ' ';
        $new_json = ' ';
        $indent_level = 0;
        $in_string = false;

        $json = trim($json);
        $json_obj = json_decode($json);

        if ($json_obj === false) {
            return false;
        }

        $json = json_encode($json_obj);
        $len = mb_strlen($json);

        for ($c = 0; $c < $len; ++$c) {
            $char = $json[$c];

            switch ($char) {
                case '{':
                case '[':
                    if (!$in_string) {
                        $new_json .= $char . "\n" . str_repeat($tab, $indent_level + 1);
                        ++$indent_level;
                    } else {
                        $new_json .= $char;
                    }

                    break;

                case '}':
                case ']':
                    if (!$in_string) {
                        --$indent_level;
                        $new_json .= "\n" . str_repeat($tab, $indent_level) . $char;
                    } else {
                        $new_json .= $char;
                    }

                    break;

                case ',':
                    if (!$in_string) {
                        $new_json .= ",\n" . str_repeat($tab, $indent_level);
                    } else {
                        $new_json .= $char;
                    }

                    break;

                case ':':
                    if (!$in_string) {
                        $new_json .= ': ';
                    } else {
                        $new_json .= $char;
                    }

                    break;

                case '"':
                    if ($c > 0 && $json[$c - 1] !== '\\') {
                        $in_string = !$in_string;
                    }
                    // no break
                default:
                    $new_json .= $char;

                    break;
            }
        }

        // Strip out the ugly characters to make the JSON easier on the eyes
        $new_json = str_replace('{', '', $new_json);
        $new_json = str_replace('},', '', $new_json);
        $new_json = str_replace('}', '', $new_json);
        $new_json = str_replace('"', '', $new_json);
        $new_json = str_replace(',', '', $new_json);

        return str_replace('\\/', '/', $new_json);
    }
}
