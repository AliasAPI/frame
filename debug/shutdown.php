<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

use AliasAPI\Debug;

/**
 * Checks for a fatal error, work around for set_error_handler not working on fatal errors.
 * This shutdown() function is used by configure_errors().
 */
function shutdown(): void
{
    $error = \error_get_last();

    if (isset($error['type'])
        && ($error['type'] === E_ERROR
        || $error['type'] === E_PARSE
        || $error['type'] === E_USER_ERROR
        || $error['type'] === E_CORE_ERROR
        || $error['type'] === E_COMPILE_ERROR)) {
        Debug\handle_error($error['type'], $error['message'], $error['file'], $error['line']);
    }
}
