<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

/**
 * Does a backtrace from previous levels.
 *
 * @param int $level levels to be backtraced
 *
 * @return string returns the class,function and file
 */
function backtrace(int $level): string
{
    $referrer = \debug_backtrace();

    if (isset($referrer[$level]['class']) && !empty($referrer[$level]['class'])) {
        $class = $referrer[$level]['class'] . '->';
    } else {
        $class = '';
    }

    if (isset($referrer[$level]['function']) && !empty($referrer[$level]['function'])) {
        $function = $referrer[$level]['function'] . '()';
    } else {
        $function = '';
    }

    if (empty($function) && isset($referrer[$level - 1]['file'])) {
        $file = \basename($referrer[$level - 1]['file']);
    } else {
        $file = '';
    }

    return $class . $function . $file;
}
