<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

use AliasAPI\Debug;

/**
 * Generates the log of the errors.
 */
function log(array $message, int $debug_level = 0): void
{
    if (!isset($GLOBALS['only_set_in_debug_log'])) {
        $GLOBALS['only_set_in_debug_log'] = [];
    }

    // The higher the debug_detail_level, the more detailed the comments get.
    if (\defined('DEBUG_MAX')
        && DEBUG_MAX > 0
        && $debug_level <= DEBUG_MAX) {
        // Get the referring class method or function
        $source = Debug\backtrace(3);

        $string = \print_r($message, true);

        $log_message = "{$source} {$string}";

        // Just load into memory until processing is done
        $GLOBALS['only_set_in_debug_log'][] = $log_message;
    }
}
