<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

use AliasAPI\Messages;

/**
 * Catches uncaught exceptions and converts them into $message.
 */
function catch_exception(object $ex): void
{
    $exception = [
        'CLASS' => \get_class($ex) . '()',
        'MESSAGE' => $ex->getMessage(),
        'FILE' => stripslashes($ex->getFile()),
        'LINE' => $ex->getLine()
    ];

    Messages\respond(500, $exception);
}
