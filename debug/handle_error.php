<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

use AliasAPI\Debug;

/**
 * Gets the error message to be returned with Messages\respond().
 *
 * @param int    $error_number
 * @param string $message
 * @param string $file
 * @param int    $line
 */
function handle_error($error_number, $message, $file, $line): bool
{
    Debug\catch_exception(new \ErrorException($message, 0, $error_number, $file, $line));

    return false;
}
