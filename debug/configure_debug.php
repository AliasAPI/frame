<?php

declare(strict_types=1);

namespace AliasAPI\Debug;

/**
 * Configures the error messages to be displayed or suppressed.
 *
 * @return array $train
 */
function configure_debug(array $train): array
{
    if (!isset($train['settings']['environment'])) {
        $train['settings']['environment'] = 'development';
    }

    if (!isset($train['settings']['debug_level'])) {
        $train['settings']['debug_level'] = 10;
    }

    if (!isset($train['settings']['debug_max'])
        || !\is_int($train['settings']['debug_max'])) {
        $train['settings']['debug_max'] = 10;
    }

    \defined('DEBUG_MAX') || \define('DEBUG_MAX', $train['settings']['debug_max']);

    \defined('E_STRICT') || \define('E_STRICT', 2048);
    \defined('E_RECOVERABLE_ERROR') || \define('E_RECOVERABLE_ERROR', 4096);
    \defined('E_DEPRECATED') || \define('E_DEPRECATED', 8192);
    \defined('E_USER_DEPRECATED') || \define('E_USER_DEPRECATED', 16384);
    // \ini_set('max_execution_time',1 );

    // todo:: Fix the https://github.com/phpstan/phpstan/issues/3188 error
    \set_error_handler('\\AliasAPI\\Debug\\handle_error');
    \set_exception_handler('\\AliasAPI\\Debug\\catch_exception');
    \register_shutdown_function('\\AliasAPI\\Debug\\shutdown');

    \ini_set('display_errors', '0');
    \ini_set('display_startup_errors', '0');
    \ini_set('log_errors', '0');
    \ini_set('error_reporting', '0');
    \error_reporting(0);

    if (is_callable('xdebug_disable')) {
        \xdebug_disable();
    }

    return $train;
}
