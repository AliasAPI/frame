<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages;

/**
 * Sets the pair in the train for the pair file.
 *
 * @param array $train            The train request
 * @param array $alias_attributes The alias attributes
 * @param array $services         The services
 *
 * @return array $train             Returns the train with the pair
 */
function set_pre_pair($train, $alias_attributes, $services = []): array
{
    if (!\defined('CONFIGPATH')) {
        Messages\respond(501, ['The CONFIGPATH has not been defined.']);
    }

    if (!isset($train['pair']['client'])
        || $train['pair']['client'] === '') {
        Messages\respond(400, ['The pair client is not set in the request.']);
    }

    if (!isset($train['pair']['server'])
        || $train['pair']['client'] === '') {
        Messages\respond(400, ['The pair server is not set in the request.']);
    }

    $client = $train['pair']['client'];

    $server = $train['pair']['server'];

    if ($client === $alias_attributes['alias']) {
        $train['settings']['clientside'] = true;
        $train['settings']['pairpath'] = CONFIGPATH . \mb_strtolower($client . '/');
    } else {
        $train['settings']['clientside'] = false;
        $train['settings']['pairpath'] = CONFIGPATH . \mb_strtolower($server . '/');
    }

    if ($train['settings']['clientside'] === true) {
        if (!isset($services[$client])) {
            Messages\respond(501, ["Alias [{$client}] is not configured in services."]);
        }

        if (!isset($services[$server])) {
            Messages\respond(501, ["Alias [{$server}] is not configured in services."]);
        }

        if (!isset($services[$server]['api_pass'])
            || $services[$server]['api_pass'] === '') {
            Messages\respond(501, ["The api pass for [{$server}] is not set in services."]);
        }

        if (!isset($services[$client]['url'])
            || $services[$client]['url'] === '') {
            Messages\respond(501, ["Manually add the URL for [{$client}] in .services.json."]);
        } else {
            $train['pair']['client_url'] = $services[$client]['url'];
        }

        if (!isset($services[$server]['url'])
            || $services[$server]['url'] === '') {
            Messages\respond(501, ["Manually add the URL for [{$server}] in .services.json."]);
        } else {
            $train['pair']['server_url'] = $services[$server]['url'];
        }
    }

    $train['settings']['pairfile'] = \mb_strtolower($client . '.' . $server);

    if (\array_key_exists('action', $train)
        && $train['action'] !== 'setup services'
        && $train['action'] !== 'delete pair file'
        && !\file_exists($train['settings']['pairpath'] . $train['settings']['pairfile'])) {
        $train['action'] = 'create pair file';
    }

    if (\array_key_exists('action', $train)
        && $train['action'] === 'create pair file') {
        $train['pair']['pairing'] = true;
    } else {
        return $train;
        // All logic below this line is $train['pair']['pairing'] = true;
    }

    if (!isset($train['pair']['client_url'])
        || \mb_strlen($train['pair']['client_url']) < 5) {
        $tr = print_r($train, true);
        Messages\respond(400, ["The client url is not set in the request. {$tr}"]);
    }

    if (!isset($train['pair']['server_url'])
        || \mb_strlen($train['pair']['server_url']) < 5) {
        Messages\respond(400, ['The server url is not set in the request.']);
    }

    if ($train['settings']['clientside'] === true
        && !isset($train['pair']['server_public_key'])) {
        // Only set the client_public_key initially and NOT after the serverside responds
        $train['pair']['client_public_key'] = $alias_attributes['api_keys']['public_key'];
    }

    // The serverside api must receive the client_public_key to pair
    if (!isset($train['pair']['client_public_key'])
        || empty($train['pair']['client_public_key'])) {
        Messages\respond(400, ['The client public key is not in the request.']);
    }

    if ($train['settings']['clientside'] === false) {
        // The serverside responds to the client pair request with the server_public_key
        $train['pair']['server_public_key'] = $alias_attributes['api_keys']['public_key'];

        // The serverside sets whether or not transmissions should be signed
        $train['pair']['sign'] = $alias_attributes['crypto_config']['sign'];

        // The serverside sets whether or not transmissions should be encrypted
        $train['pair']['encrypt'] = $alias_attributes['crypto_config']['encrypt'];

        $train['pair']['datetime'] = \date('Y-m-d H:i:s');

        $train['pair']['pairfile'] = $train['settings']['pairfile'];

        // Do NOT allow the client and server to encrypt & sign with the same public keys
        if ($train['pair']['client_public_key'] === $train['pair']['server_public_key']) {
            Messages\respond(400, ['The client and server public keys match.']);
        }
    }

    return $train;
}
