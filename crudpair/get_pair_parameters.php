<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

/**
 * Returns the pair parameters or an empty array.
 *
 * return $pair_parameters  The encryption settings between client and server
 */
function get_pair_parameters(): ?array
{
    if (!isset($GLOBALS['only_set_in_crudpair_set_pair_parameters'])) {
        return [];
    }

    return $GLOBALS['only_set_in_crudpair_set_pair_parameters'];
}
