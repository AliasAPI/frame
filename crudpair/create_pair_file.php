<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson;
use AliasAPI\Crypto;
use AliasAPI\Messages;

/**
 * Creates a pair file from the request.
 *
 * @param array $train The request
 *
 * @return void Responds with a 501 error 501 if the pair file is not created
 */
function create_pair_file(array $train): void
{
    if (!\array_key_exists('action', $train)
        || $train['action'] !== 'create pair file'
        || !\array_key_exists('client', $train['pair'])
        || !\array_key_exists('server', $train['pair'])
        || !\array_key_exists('client_url', $train['pair'])
        || !\array_key_exists('server_url', $train['pair'])
        || !\array_key_exists('client_public_key', $train['pair'])
        // The serverside returns server_public_key
        || !\array_key_exists('server_public_key', $train['pair'])
        // sign and encrypt are set serverside first
        || !\array_key_exists('sign', $train['pair'])
        || !\array_key_exists('encrypt', $train['pair'])
        || !\array_key_exists('pairfile', $train['pair'])
        || !\array_key_exists('datetime', $train['pair'])
        || !\array_key_exists('pairpath', $train['settings'])) {
        return;
    }

    $file = [];

    // Create consistent file formatting
    $keys = [
        'client',
        'server',
        'client_url',
        'server_url',
        'client_public_key',
        'server_public_key',
        'shared_key',
        'sign',
        'encrypt',
        'pairfile',
        'datetime'
    ];

    foreach ($keys as $index => $key) {
        if (\array_key_exists($key, $train['pair'])) {
            $file[$key] = $train['pair'][$key];
        }
    }

    // Create a secret shared symmetric key
    $file['shared_key'] = Crypto\derive_paseto_shared_key($file);

    // Create the subdirectory (if needed) and write create JSON pair file
    $created = CrudJson\create_json_file($train['settings']['pairpath'], $file['pairfile'], $file);

    if ($created !== true) {
        Messages\respond(501, ['There was a problem creating the pair file.']);
    }
}
