<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudPair;

/**
 * Calls the CrudPair functions in the correct sequence.
 */
function track(array $train, array $alias_attributes, array $services = []): array
{
    $train = CrudPair\set_train($train);

    $train = CrudPair\set_pre_pair($train, $alias_attributes, $services);

    CrudPair\set_pair_auth($train, $services);

    $pair_response = CrudPair\send_pair_request($train);

    $train = CrudPair\set_pair_response($train, $pair_response);

    CrudPair\delete_pair_file($train);

    CrudPair\create_pair_file($train);

    $pair_file = CrudPair\read_pair_file($train);

    $train = CrudPair\get_train($train);

    $pair_parameters = CrudPair\set_pair_parameters($train, $pair_file);

    return CrudPair\set_request_pair($train, $pair_parameters);
}
