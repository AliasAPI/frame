<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages;

/**
 * Sets the parameters for the client server pair.
 *
 * @param array $train
 * @param array $pair_file
 */
function set_pair_parameters($train, $pair_file): array
{
    $params = [];

    $keys = [
        'client',
        'server',
        'client_url',
        'server_url',
        'client_public_key',
        'server_public_key',
        'shared_key',
        'sign',
        'encrypt',
        'pairfile',
        'datetime',
        'shared_key'
    ];

    foreach ($keys as $index => $key) {
        if (\array_key_exists($key, $pair_file)) {
            $params[$key] = $pair_file[$key];
        } elseif (\array_key_exists($key, $train['pair'])) {
            $params[$key] = $train['pair'][$key];
        }
    }

    // todo:: Add error checking to set_pair_parameters.php

    // if (! \array_key_exists($pair_file['encrypt'])) {
    //     Messages\respond(501, ["The pair encrypt setting is missing."]);
    // }

    // if (! \array_key_exists($pair_file['sign'])) {
    //     Messages\respond(501, ["The message sign setting is missing."]);
    // }

    // if ($pair_file['sign'] == true
    //     && ! \array_key_exists($alias_attributes['api_keys']['private_key'])) {
    //     Messages\respond(501, ["The private_key is not available."]);
    // }

    // if ($pair_file['encrypt'] == true
    //     && ! \array_key_exists($pair_file['shared_key'])) {
    //     Messages\respond(501, ["The shared_key is not available."]);
    // }

    // if ($pair_file['sign'] == true
    //     && ! \array_key_exists($pair_file['client_public_key'])) {
    //     Messages\respond(501, ["The client_public_key is not available."]);
    // }

    // if ($pair_file['encrypt'] == true
    //     && ! \array_key_exists($pair_file['shared_key'])) {
    //     Messages\respond(501, ["The shared_key is not available."]);
    // }

    $GLOBALS['only_set_in_crudpair_set_pair_parameters'] = $params;

    return $GLOBALS['only_set_in_crudpair_set_pair_parameters'];
}
