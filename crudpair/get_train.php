<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

/**
 * Gets the train request or pair_response.
 *
 * @return array $train
 */
function get_train(array $train): array
{
    if (\array_key_exists('only_set_in_crudpair_set_train', $GLOBALS)
        && $train['action'] === 'create pair file') {
        $train = $GLOBALS['only_set_in_crudpair_set_train'];
    }

    return $train;
}
