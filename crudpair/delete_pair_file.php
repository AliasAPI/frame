<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * Deletes the pair file.
 *
 * @param array $train the train request
 *
 * @return void Returns an error 500 if deletion of pair file failed.
 *              Returns 200 response if pair file has been deleted.
 */
function delete_pair_file(array $train): void
{
    if (!\array_key_exists('action', $train)
        || $train['action'] !== 'delete pair file') {
        return;
    }

    $deleted = CrudJson\delete_json_file(
        $train['settings']['pairpath'],
        $train['settings']['pairfile']
    );

    if ($deleted === false) {
        Messages\respond(500, ['Deleting the pair file failed.']);
    }

    if ($deleted === true
        && \array_key_exists('clientside', $train['settings'])
        // If the server is responding to the client
        && $train['settings']['clientside'] === false) {
        Messages\respond(200, ['The pair file has been deleted.']);
    }
}
