<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages;

/**
 * Sends the pair request to the server and returns the response.
 *
 * @return array $pair_response  Returns the pair_response from the serverside or empty
 *               The response includes server_public_key, sign, encrypt
 */
function send_pair_request(array $train): array
{
    if ($train['action'] !== 'create pair file'
        || $train['settings']['clientside'] !== true) {
        return [];
    }

    unset($train['inputs']);

    $tag = Messages\set_request_tag($train, false);

    $method = Messages\set_method();

    $server_url = Messages\set_server_url($train['pair']['server_url']);

    $body = Messages\set_body($train);

    $headers = Messages\set_headers([], $body);

    $pair_response = Messages\request();

    if (\array_key_exists('body', $pair_response) 
	&& \is_array($pair_response['body'])) {
        $pair_response = \array_merge($pair_response, $pair_response['body']);
        unset($pair_response['body']);
    }

    return $pair_response;
}
