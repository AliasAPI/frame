<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

/**
 * Sets the train request so it can be retrieved after create pair.
 *
 * @param array $train
 *
 * @return array $train
 */
function set_train($train): array
{
    $GLOBALS['only_set_in_crudpair_set_train'] = $train;

    return $GLOBALS['only_set_in_crudpair_set_train'];
}
