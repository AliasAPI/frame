<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks the pair response from the server side.
 *
 * @param array $train
 * @param array $pair_response The response from the serverside
 *
 * @return array $train
 */
function set_pair_response($train, $pair_response): array
{
    if (!\array_key_exists('action', $pair_response)
        || $pair_response['action'] !== 'create pair file') {
        return $train;
    }

    $options = [
        'check' => 'array',
        'nullable' => false,
        'filter_values' => false,
        'required_keys' => [
            'status_code',
            'reason',
            'tag',
            'action',
            'pair'
        ],
        'required_values' => [
            'status_code',
            'reason',
            'tag',
            'action',
            'pair'
        ]
    ];

    if (!Check\check_array('pair_response', $pair_response, $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'integer',
        'nullable' => false,
        'range' => [
            'min' => 200,
            'max' => 399
        ]
    ];

    if (!Check\check_integer('status_code', $pair_response['status_code'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'string',
        'nullable' => false,
        'range' => [
            'min' => 2,
            'max' => 100
        ]
    ];

    if (!Check\check_string('reason', $pair_response['reason'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if ($pair_response['tag'] !== Messages\get_request_tag()) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'array',
        'nullable' => false,
        'filter_values' => true,
        'required_keys' => [
            'client',
            'server',
            'client_url',
            'server_url',
            'client_public_key',
            'server_public_key',
            'sign',
            'encrypt',
            'datetime',
            'pairfile'
        ],
        'required_values' => [
            'client',
            'server',
            'client_url',
            'server_url',
            'client_public_key',
            'server_public_key',
            'sign',
            'encrypt',
            'datetime',
            'pairfile'
        ]
    ];

    $pair = $pair_response['pair'];

    if (!Check\check_array('pair', $pair, $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'string',
        'nullable' => false,
        'range' => [
            'min' => 2,
            'max' => 255
        ]
    ];

    if (!Check\check_string('client', $pair['client'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if (!Check\check_string('server', $pair['server'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if ($pair['client'] !== $train['pair']['client']) {
        Messages\respond(500, ['The pair client response does not match']);
    }

    if ($pair['server'] !== $train['pair']['server']) {
        Messages\respond(400, ['The pair server response does not match']);
    }

    $options = [
        'check' => 'url',
        'nullable' => false
    ];

    if (!Check\check_url('client_url', $pair['client_url'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if (!Check\check_url('server_url', $pair['server_url'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'string',
        'nullable' => false,
        'range' => [
            'min' => 2,
            'max' => 255
        ]
    ];

    if (!Check\check_string('client_public_key', $pair['client_public_key'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if (!Check\check_string('server_public_key', $pair['server_public_key'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if (!Check\check_string('pairfile', $pair['pairfile'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'boolean',
        'nullable' => false,
    ];

    if (!Check\check_boolean('sign', $pair['sign'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if (!Check\check_boolean('encrypt', $pair['encrypt'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    $options = [
        'check' => 'datetime',
        'nullable' => false,
        'format' => 'Y-m-d H:i:s'
    ];

    if (!Check\check_datetime('datetime', $pair['datetime'], $options)) {
        Messages\respond(500, Messages\get_reply(400, 600, 10));
    }

    if ($pair['pairfile'] !== $train['settings']['pairfile']) {
        Messages\respond(400, ['The pair file response does not match']);
    }

    if (\array_key_exists('shared_key', $pair)) {
        Messages\respond(400, ['The pair shared_key exists']);
    }

    // Get the values from Check\get_key_values() since they are santized
    $response = [];
    $response['status_code'] = Check\get_key_value('status_code');
    $response['reason'] = Check\get_key_value('reason');
    $response['tag'] = $pair_response['tag'];
    $response['action'] = $pair_response['action'];
    $response['pair']['client'] = Check\get_key_value('client');
    $response['pair']['server'] = Check\get_key_value('server');
    $response['pair']['client_url'] = Check\get_key_value('client_url');
    $response['pair']['server_url'] = Check\get_key_value('server_url');
    $response['pair']['client_public_key'] = Check\get_key_value('client_public_key');
    $response['pair']['server_public_key'] = Check\get_key_value('server_public_key');
    $response['pair']['sign'] = Check\get_key_value('sign');
    $response['pair']['encrypt'] = Check\get_key_value('encrypt');
    $response['pair']['datetime'] = Check\get_key_value('datetime');
    $response['pair']['pairfile'] = Check\get_key_value('pairfile');
    $response['settings']['pairpath'] = $train['settings']['pairpath'];
    $response['settings']['pairfile'] = Check\get_key_value('pairfile');

    return $response;
}
