<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * Reads the pair file.
 *
 * @return array $pair_file   Returns the JSON pair_file as an array
 */
function read_pair_file(array $train): array
{
    $pair_file = [];

    if (\array_key_exists('action', $train)
        && ($train['action'] === 'delete pair file'
        || $train['action'] === 'setup services')) {
        return $train;
    }

    $pair_file = CrudJson\read_json_file(
        $train['settings']['pairpath'],
        $train['settings']['pairfile']
    );

    if (\array_key_exists('clientside', $train['settings'])
        && $train['settings']['clientside'] === false
        && \array_key_exists('action', $train)
        && $train['action'] === 'create pair file'
        // The server side has the server_public_key set
        && \array_key_exists('server_public_key', $pair_file)) {
        // Do not transmit the secret shared symetric key
        if (\array_key_exists('shared_key', $pair_file)) {
            unset($pair_file['shared_key']);
        }

        $response = [];
        $response['action'] = $train['action'];
        $response['pair'] = $pair_file;

        // Return the clean_pair to client
        Messages\respond(201, $response);
    }

    // Return the pair
    return $pair_file;
}
