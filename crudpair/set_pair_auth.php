<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages;

/**
 * Authorizes the pair file.
 *
 * @param array $train
 * @param array $services Defines the client and server
 *
 * @return void returns an error 403 response if the pair is not set properly
 */
function set_pair_auth($train, $services = []): void
{
    // The api_user and api_pass are only required when pairing a client and server
    if (\array_key_exists('pairing', $train['pair'])
        && $train['pair']['pairing'] === true
        && \array_key_exists('clientside', $train['settings'])
        && $train['settings']['clientside'] === true
        && \array_key_exists('alias', $services[$train['pair']['server']])
        && \array_key_exists('api_pass', $services[$train['pair']['server']])) {
        // api_user and api_pass are added to the request later
        Messages\set_auth(
            $services[$train['pair']['server']]['alias'],
            $services[$train['pair']['server']]['api_pass']
        );
    }

    // the api_user and api_pass are validated in the alias track
    if (\array_key_exists('pairing', $train['pair'])
        && $train['pair']['pairing'] === true
        && \array_key_exists('clientside', $train['settings'])
        && $train['settings']['clientside'] === false) {
        $credentials = Messages\get_request_credentials();

        if (!\array_key_exists('api_user', $credentials)) {
            Messages\respond(403, ['The api_user is not set in auth.']);
        }

        // auth pass is set when the client is preparing to send a request
        if (!\array_key_exists('api_pass', $credentials)) {
            Messages\respond(403, ['The api_pass is not set in auth.']);
        }
    }
}
