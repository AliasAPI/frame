<?php

declare(strict_types=1);

namespace AliasAPI\CrudPair;

use AliasAPI\Messages;

/**
 * Sets the pair for the request.
 *
 * @param array $train
 * @param array $pair_parameters
 *
 * @return array $train
 */
function set_request_pair($train, $pair_parameters): array
{
    if (!\array_key_exists('action', $train)) {
        Messages\respond(500, ['The action is not set in set_request_pair.']);
    }

    if ($train['action'] === 'create pair file'
        || $train['action'] === 'read pair file') {
        // Pairing; Delete the request inputs array
        unset($train['inputs'], $pair_parameters['shared_key']);
    } else {
        unset($pair_parameters['client_url'], $pair_parameters['server_url'], $pair_parameters['client_public_key'], $pair_parameters['server_public_key'], $pair_parameters['sign'], $pair_parameters['encrypt'], $pair_parameters['pairfile'], $pair_parameters['datetime'], $pair_parameters['shared_key']);
    }

    $train['pair'] = $pair_parameters;

    return $train;
}
