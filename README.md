AliasAPI README

WHAT:
AliasAPI is an API proxy micro microservice framework written in procedural PHP.
It is a Composer package that is used by several loosely related microservices.
The microservice architecture is one-to-many that relies on a stateless gateway.

WHY:
AliasAPI is simple, stable, small, swift, specific, structured and secure.

    Simple
    It uses procedural code which is easier to understand by stakeholders.

    Stable
    The procedural code is encapsulated in top level, easy to test functions.

    Structured
    Each directory contains files using namespaces matching the directory names.
    Each file contains a single function that matches the filename.
    Each function name starts with a verb to describe the action it completes.
    Most directories contain a file named "track" that calls the functions.

    Small
    The AliasAPI footprint is small so that it works on free shared hosts.
    The functionality is limited to facilitating client-server transmissions.

    Swift
    The AliasAPI framework makes development time more swift and simple.
    A microservice is created by adding a few functions to the framework.
    PHP libraries are simplified since they are called by existing code.

    Specific
    AliasAPI calls specific functionality from the often bloated libraries.
    Typically, the logic is limited to create, read, update, and delete.

    Secure
    The client-server transmissions are signed and encrypted.
    The security relies on a library developed by security experts.

WHERE:
    Packagist https://packagist.org/packages/aliasapi/frame
    BitBucket https://bitbucket.org/AliasAPI/framework/src/master

HOW:
To install AliasAPI, download and install a library that relies on AliasAPI. :)
Or, include "aliasapi/frame": "dev-master" in the composer.json of your project.
Or, run git clone git@bitbucket.org:AliasAPI/frame.git
