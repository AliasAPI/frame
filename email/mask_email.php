<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;

/**
 * Replaces characters in an email address to mask it for security reasons.
 *
 * @param string $email The email address to mask
 *
 * @return string $masked_email
 */
function mask_email(string $email): string
{
    Check\check_email('email', $email, []);

    // Regular expression to make a masked_email looks like this d**********r@g****l.com
    $regex = '/(?<=.)[^@\n](?=[^@\n]*?@)|(?:(?<=@.)|(?!^)\G(?=[^@\n]*$)).(?=.*[^@\n]\.)/m';

    // Replace the original characters with the * character
    $sub_text = '*';

    return \preg_replace($regex, $sub_text, $email);
}
