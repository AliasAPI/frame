<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;
use AliasAPI\Messages;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

/**
 * Sends an email and then saves it in the email sent folder.
 *
 * https://github.com/PHPMailer/PHPMailer/blob/master/examples/gmail.phps
 * https://github.com/PHPMailer/PHPMailer/blob/master/get_oauth_token.php
 *
 * @return array $train
 */
function send_email(array $train): array
{
    if (!\class_exists('PHPMailer\PHPMailer\PHPMailer')) {
        Messages\respond(501, ['The PHPMailer library is not installed yet.']);
    }

    // Create a new PHPMailer instance
    $mail = new PHPMailer();

    // Tell PHPMailer to use SMTP
    $mail->isSMTP();

    if ($train['alias_attributes']['debug_config']['environment'] !== 'development') {
        // Disable SMTP debugging for production use
        $mail->SMTPDebug = SMTP::DEBUG_OFF;
    } else {
        // Enable client and server debugging messages
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
    }

    // Set the hostname of the mail server
    $mail->Host = $train['alias_attributes']['email_config']['api_url'];

    // Use `$mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6, but it may cause issues with TLS

    // Set the SMTP port number:
    // 465 for SMTP with implicit TLS, a.k.a. RFC8314 SMTPS or 587 for SMTP+STARTTLS
    $mail->Port = $train['alias_attributes']['email_config']['email_port'];

    // Set the encryption mechanism to use:
    if ($train['alias_attributes']['email_config']['email_port'] === 465) {
        // SMTPS (implicit TLS on port 465)
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    } elseif ($train['alias_attributes']['email_config']['email_port'] === 587) {
        // STARTTLS (explicit TLS on port 587)
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    }

    // If the username and password are set, use SMTP authentication
    if ($train['alias_attributes']['email_config']['username'] !== ''
       && $train['alias_attributes']['email_config']['password'] !== '') {
        $mail->SMTPAuth = true;
    } else {
        $mail->SMTPAuth = false;
    }

    // Username to use for SMTP authentication
    $mail->Username = $train['alias_attributes']['email_config']['username'];

    // Password to use for SMTP authentication
    $mail->Password = $train['alias_attributes']['email_config']['password'];

    // Set who the message is to be sent from
    // Note that with gmail you can only use your account address (same as `Username`)
    // or predefined aliases that you have configured within your account.
    // Do not use user-submitted addresses here
    $mail->setFrom(
        $train['email_attributes']['from_email'],
        $train['email_attributes']['from_name']
    );

    // Set an alternative reply-to address
    // This is a good place to put user-submitted addresses
    $mail->addReplyTo(
        $train['email_attributes']['reply_to_email'],
        $train['email_attributes']['reply_to_name']
    );

    // Set who the message is to be sent to
    $mail->addAddress(
        $train['email_attributes']['to_email'],
        $train['email_attributes']['to_name']
    );

    // Set the subject line
    $mail->Subject = $train['email_attributes']['subject'];

    // Set the html body
    $mail->msgHTML($train['email_attributes']['body_html']);

    // Set the plain text body
    $mail->AltBody = $train['email_attributes']['body_text'];

    // Set the attachments (if they exist)
    if (\is_array($train['email_attributes']['attachments'])) {
        foreach ($train['email_attributes']['attachments'] as $file_name => $path) {
            $mail->addAttachment($path . '/' . $file_name, $file_name);
        }
    }

    // If it is too early to send the next email, return
    if (\strtotime($train['email_attributes']['date_next_email']) < \time()) {
        $train['email_attributes']['email_sent'] = false;

        return $train;
    }

    // Send the email (and get back a true or false response)
    $train['email_attributes']['email_sent'] = $mail->send();

    // Check for email errors
    if ($train['email_attributes']['email_sent'] === false) {
        $train['email_attributes']['email_error'] = $mail->ErrorInfo;
    }

    // If the email was sent, move the email to the sent folder
    if ($train['email_attributes']['email_sent'] === true) {
        // Make sure the IMAP functions exist on the server
        if (!\function_exists('imap_open')
            || !\function_exists('imap_append')
            || !\function_exists('imap_close')) {
            return $train;
        }

        $imapStream = \imap_open(
            // Set the location of the default Sent Mail folder
            $train['alias_attributes']['email_config']['sent_url'],
            $train['alias_attributes']['email_config']['username'],
            $train['alias_attributes']['email_config']['password']
        );

        $result = \imap_append(
            $imapStream,
            $train['alias_attributes']['email_config']['sent_url'],
            $mail->getSentMIMEMessage()
        );

        \imap_close($imapStream);
    }

    return $train;
}
