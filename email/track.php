<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Email;

/**
 * Sets all of the attributes and sends an email.
 *
 * @param array $train The $train request
 *
 * @return array $train   The modified $train
 */
function track(array $train): array
{
    Email\check_email_configuration($train);

    Email\check_email_attributes($train);

    $train = Email\set_date_last_email($train);

    $train = Email\set_delay_email($train);

    $train = Email\set_date_next_email($train);

    $train = Email\request_email_body($train);

    $train = Email\replace_body_tags($train);

    $train = Email\send_email($train);

    $train = Email\set_date_last_email($train);

    $train = Email\update_rows_if_email_sent($train);

    return Email\set_result_message($train);
}
