<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;

/**
 * Replaces the tags in the html and text email message bodies.
 *
 * The email templates use tags that look like this: $tag
 * The tags are replaced using the replacement_tags array
 * Attachments should just be links within the email body
 *
 * @return array $train with the body_html and body_text messages updated
 */
function replace_body_tags(array $train): array
{
    $replacement_tags = [];

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['body_html'],
            'required_values' => ['body_html']]
    );

    Check\check_string(
        'body_html',
        $train['email_attributes']['body_html'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 10,
                'max' => 100000]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['body_text'],
            'required_values' => ['body_text']]
    );

    Check\check_string(
        'body_text',
        $train['email_attributes']['body_text'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 10,
                'max' => 100000]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['replacement_tags'],
            'required_values' => []]
    );

    if (\count($train['email_attributes']['replacement_tags']) > 0) {
        foreach ($train['email_attributes']['replacement_tags'] as $tag => $string) {
            $replacement_tags['$' . $tag] = \filter_var($string, FILTER_SANITIZE_STRING);
        }

        $train['email_attributes']['body_html'] =
            \strtr($train['email_attributes']['body_html'], $replacement_tags);

        $train['email_attributes']['body_text'] =
            \strtr($train['email_attributes']['body_text'], $replacement_tags);
    }

    return $train;
}
