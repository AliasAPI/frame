<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;

/**
 * Checks all of the email attributes used to compose an email.
 */
function check_email_attributes(array $train): void
{
    Check\check_array(
        'email_attributes',
        $train,
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['email_attributes'],
            'required_values' => ['email_attributes']]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['from_name'],
            'required_values' => ['from_name']]
    );

    Check\check_string(
        'from_name',
        $train['email_attributes']['from_name'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 5, 'max' => 50]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['from_email'],
            'required_values' => ['from_email']]
    );

    Check\check_email(
        'from_email',
        $train['email_attributes']['from_email'],
        ['check' => 'email',
            'nullable' => false]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['reply_to_name'],
            'required_values' => ['reply_to_name']]
    );

    Check\check_string(
        'reply_to_name',
        $train['email_attributes']['reply_to_name'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 5, 'max' => 50]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['reply_to_email'],
            'required_values' => ['reply_to_email']]
    );

    Check\check_email(
        'reply_to_email',
        $train['email_attributes']['reply_to_email'],
        ['check' => 'email',
            'nullable' => false]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['to_name'],
            'required_values' => ['to_name']]
    );

    Check\check_string(
        'to_name',
        $train['email_attributes']['to_name'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 5, 'max' => 50]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['to_email'],
            'required_values' => ['to_email']]
    );

    Check\check_email(
        'to_email',
        $train['email_attributes']['to_email'],
        ['check' => 'email',
            'nullable' => false]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['subject'],
            'required_values' => ['subject']]
    );

    Check\check_string(
        'subject',
        $train['email_attributes']['subject'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 5, 'max' => 100]]
    );
}
