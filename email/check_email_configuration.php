<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Messages;

/**
 * Configures the email system using the values set in alias_attributes (.alias.json).
 */
function check_email_configuration(array $train): void
{
    if (!\array_key_exists('alias_attributes', $train)) {
        Messages\respond(501, ['The alias_attributes are not in the train.']);
    }

    if (!\array_key_exists('email_config', $train['alias_attributes'])) {
        Messages\respond(501, ['The Alias email_config has not been configured correctly.']);
    }

    if (!\array_key_exists('api_url', $train['alias_attributes']['email_config'])
        || $train['alias_attributes']['email_config']['api_url'] === '') {
        Messages\respond(501, ['Manually add the email_config api_url to the .alias.json.']);
    }

    if (!\array_key_exists('username', $train['alias_attributes']['email_config'])
        || $train['alias_attributes']['email_config']['api_url'] === '') {
        Messages\respond(501, ['Manually add the email_config username to the .alias.json.']);
    }

    if (!\array_key_exists('password', $train['alias_attributes']['email_config'])
        || $train['alias_attributes']['email_config']['api_url'] === '') {
        Messages\respond(501, ['Manually add the email_config password to the .alias.json.']);
    }

    if (!\array_key_exists('email_port', $train['alias_attributes']['email_config'])
        || ($train['alias_attributes']['email_config']['email_port'] !== 465
            && $train['alias_attributes']['email_config']['email_port'] !== 587)) {
        Messages\respond(501, ['Manually add the email_config email_port to the .alias.json.']);
    }

    if (!\array_key_exists('send_email', $train['alias_attributes']['email_config'])
        || !\is_bool($train['alias_attributes']['email_config']['send_email'])) {
        Messages\respond(501, ['Manually add the email_config send_email to the .alias.json.']);
    }

    if (!\array_key_exists('sent_url', $train['alias_attributes']['email_config'])
        || \mb_strlen($train['alias_attributes']['email_config']['sent_url']) < 5) {
        Messages\respond(501, ['Manually add the email_config sent_url to the .alias.json.']);
    }

    if (!\array_key_exists('environment', $train['alias_attributes']['debug_config'])
        || $train['alias_attributes']['debug_config']['environment'] === '') {
        Messages\respond(501, ['Manually add the email_config send_email to the .alias.json.']);
    }

    if (!\array_key_exists('environment', $train['alias_attributes']['debug_config'])
        || $train['alias_attributes']['debug_config']['environment'] === '') {
        Messages\respond(501, ['Manually add the email_config send_email to the .alias.json.']);
    }
}
