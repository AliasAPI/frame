<?php

declare(strict_types=1);

namespace AliasAPI\Email;

/**
 * Sets the minimum datetime of the next email to the to_email
 * If the date_next_email = 0000-00-00 00:00:00, the email will not be sent.
 *
 * @return array $train   The modified $train
 */
function set_date_next_email(array $train): array
{
    if (!\array_key_exists('email_attributes', $train)
        || !\array_key_exists('date_last_email', $train['email_attributes'])
        || !\array_key_exists('delay_email', $train['email_attributes'])) {
        return $train;
    }

    if ($train['alias_attributes']['email_config']['send_email'] !== true) {
        $train['email_attributes']['date_next_email'] = '0000-00-00 00:00:00';

        return $train;
    }

    // If date_next_email has already been calculated and provided, simply use it
    if (\array_key_exists('date_next_email', $train['email_attributes'])
        && $train['email_attributes']['date_next_email'] > '0000-00-00 00:00:00') {
        return $train;
    }

    if ($train['email_attributes']['date_last_email'] === '0000-00-00 00:00:00') {
        $train['email_attributes']['date_next_email'] = \gmdate('Y-m-d H:i:s', \time());

        return $train;
    }

    if ($train['email_attributes']['date_last_email'] > '0000-00-00 00:00:00') {
        $train['email_attributes']['date_next_email'] = \gmdate(
            'Y-m-d H:i:s',
            \strtotime(
                '+' . $train['email_attributes']['delay_email'] . ' minutes',
                \strtotime($train['email_attributes']['date_last_email'])
            )
        );
    }

    // GDPR compliance
    // Lawful Basis
    // Lawful Basis Date Reviewed
    // Lawful Basis Source

    return $train;
}
