<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\CrudTable;

/**
 * Updates (the date_last_email column in) a table if the configuration is set.
 *
 * @return array $train
 */
function update_rows_if_email_sent(array $train): array
{
    if (!\array_key_exists('email_attributes', $train)
        || !\array_key_exists('email_sent', $train['email_attributes'])
        || $train['email_attributes']['email_sent'] !== true
        || !\array_key_exists('table', $train['email_attributes'])
        || !\array_key_exists('update_pairs', $train['email_attributes'])
        || !\array_key_exists('where_pairs', $train['email_attributes'])) {
        return $train;
    }

    CrudTable\update_rows(
        $train['email_attributes']['table'],
        $train['email_attributes']['update_pairs'],
        $train['email_attributes']['where_pairs']
    );

    return $train;
}
