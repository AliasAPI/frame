<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;

/**
 * Sets the number of minutes that must expire before sending another email to the to_email
 * The default of 0 is set in case the server is not using a database and still sends emails.
 *
 * @return array $train   The modified $train
 */
function set_delay_email(array $train): array
{
    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['delay_email'],
            'required_values' => ['delay_email']]
    );

    // Set the default number of minutes to wait before sending another email
    if (!\array_key_exists('delay_email', $train['email_attributes'])
        || !\is_int($train['email_attributes']['delay_email'])) {
        $train['email_attributes']['delay_email'] = 0;
    }

    return $train;
}
