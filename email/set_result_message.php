<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;
use AliasAPI\Email;
use AliasAPI\Messages;

/**
 * Sets the date of the last email sent to a default datetime.
 *
 * @return array $train
 */
function set_result_message(array $train): array
{
    if (!\array_key_exists('email_sent', $train['email_attributes'])
        || !\array_key_exists('to_email', $train['email_attributes'])) {
        return $train;
    }

    Check\check_email(
        'to_email',
        $train['email_attributes']['to_email'],
        ['check' => 'email',
            'nullable' => false,
            'session' => false]
    );

    if ($train['email_attributes']['email_sent'] === true) {
        $masked_email = Email\mask_email($train['email_attributes']['to_email']);

        Messages\set_reply(201, ["An email was sent to {$masked_email}"]);
    } elseif ($train['email_attributes']['email_sent'] === false
              && \array_key_exists('email_error', $train['email_attributes'])) {
        Messages\set_reply(500, ['An email error occurred: ' .
               $train['email_attributes']['email_error']]);
    }

    return $train;
}
