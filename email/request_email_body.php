<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Requests the html and text email message bodies.
 *
 * The email templates should be stored on the frontend server
 * That way, the email messages can be updated by a marketing team
 * Attachments should just be links within the email body
 *
 * @return array $train with the body_html and body_text templates
 */
function request_email_body(array $train): array
{
    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['body_html_template_url'],
            'required_values' => []]
    );

    Check\check_string(
        'body_html_template_url',
        $train['email_attributes']['preg_match('],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 10, 'max' => 10000]]
    );

    Check\check_array(
        'email_attributes',
        $train['email_attributes']['body_text_template_url'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['body_text_template_url'],
            'required_values' => []]
    );

    Check\check_string(
        'body_text_template_url',
        $train['email_attributes']['body_text_template_url'],
        ['check' => 'string',
            'nullable' => false,
            'range' => ['min' => 10, 'max' => 10000]]
    );

    Check\check_url(
        'body_html_template_url',
        $train['email_attributes']['body_html_template_url'],
        ['check' => 'url',
            'nullable' => false,
            'session' => false]
    );

    Check\check_url(
        'body_text_template_url',
        $train['email_attributes']['body_text_template_url'],
        ['check' => 'url',
            'nullable' => false,
            'session' => false]
    );

    Messages\set_request_tag([], true);

    Messages\set_method('GET');

    Messages\set_server_url($train['email_attributes']['body_html_template_url']);

    $html = Messages\request();

    Messages\set_request_tag([], true);

    Messages\set_server_url($train['email_attributes']['body_text_template_url']);

    $text = Messages\request();

    if ($html['body'] === '') {
        Messages\respond($html['status_code'], ['The email html body could not be downloaded.']);
    }

    if ($text['body'] === '') {
        Messages\respond($text['status_code'], ['The email text body could not be downloaded.']);
    }

    $train['email_attributes']['body_html'] = $html['body'];

    $train['email_attributes']['body_text'] = $html['body'];

    return $train;
}
