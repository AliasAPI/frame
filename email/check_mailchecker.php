<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Messages;

/**
 * Checks the email against a long email domain list in /vendor/fgribreau/.
 *
 * @param string $email The email address with the domain to be checked
 *
 * @return bool true|false  Returns false if the email domain is banned
 */
function check_mailchecker(string $email): bool
{
    $who = 'MailChecker';

    // This structure satisfes phpstan
    if (\class_exists('MailChecker')) {
        if ($email === ''
            || !\MailChecker::isValid($email)) {
            Messages\set_reply(403, ["{$who} says [{$email}] is unacceptable."]);

            return false;
        }
    } else {
        Messages\respond(501, ['The MailChecker library does not exist.']);
    }

    return true;
}
