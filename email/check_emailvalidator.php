<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Messages;
use EmailValidator\EmailValidator;

/**
 * Parses the email domain and checks it against a blacklist and checkdnsrr().
 *
 * @param string $email The email address with the domain to be checked
 *
 * @return bool true|false  Returns true if the email is valid
 */
function check_emailvalidator(string $email): bool
{
    $who = 'EmailValidator';

    $config = [
        'checkMxRecords' => true,
        'checkBannedListedEmail' => true,
        'checkDisposableEmail' => true,
        'checkFreeEmail' => false,
        'bannedList' => [
            'egroups.com',
            'facebookmail.com',
            'googlegroups.com',
            'groups.msn.com',
            'onelist.com',
            'phx.gbl',
            'yahoogroups.com'
        ],
        'disposableList' => [],
        'freeList' => [],
    ];

    // This structure satisfes phpstan
    if (\class_exists('EmailValidator\EmailValidator')) {
        $emailValidator = new EmailValidator($config);

        $is_valid = $emailValidator->validate($email);

        if (!$is_valid) {
            $error = \mb_strtolower($emailValidator->getErrorReason());
            Messages\set_reply(403, ["{$who} says the email {$error}."]);

            return false;
        }
    } else {
        Messages\respond(501, ["The {$who} library does not exist."]);
    }

    return true;
}
