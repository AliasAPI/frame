<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Messages;

/**
 * Sends a GET request to Disify.com.
 *
 * @param string $email The email address with the domain to be checked
 *
 * @return bool true|false  Returns false if Disify returns an error
 */
function check_disify(string $email): bool
{
    $who = 'Disify.com';

    // If there is already an error, skip this process
    if (\count(Messages\get_reply(400, 600, 1)) > 0) {
        return false;
    }

    $url = 'https://disify.com/api/email/' . $email;

    $domain = \preg_replace('/^[^@]++@/', '', $email);

    Messages\set_method('GET');

    Messages\set_server_url($url);

    $response = Messages\request();

    if (\is_array($response['body'])
        && \array_key_exists('disposable', $response['body'])
        && $response['body']['disposable'] === true) {
        Messages\set_reply(403, ["{$who} says the [{$domain}] email is unacceptable."]);

        return false;
    }

    if (\is_array($response['body'])
        && \array_key_exists('dns', $response['body'])
        && $response['body']['dns'] === false) {
        Messages\set_reply(403, ["{$who} says [{$domain}] cannot handle emails."]);

        return false;
    }

    if (\is_array($response['body'])
        && \array_key_exists('format', $response['body'])
        && $response['body']['format'] === false) {
        Messages\set_reply(403, ["{$who} says [{$email}] has a bad format."]);

        return false;
    }

    return true;
}
