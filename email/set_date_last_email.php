<?php

declare(strict_types=1);

namespace AliasAPI\Email;

use AliasAPI\Check;

/**
 * Sets the date of the last email sent to a default datetime.
 *
 * @return array $train
 */
function set_date_last_email(array $train): array
{
    Check\check_array(
        'email_attributes',
        $train['email_attributes'],
        ['check' => 'array',
            'nullable' => false,
            'filter_values' => false,
            'required_keys' => ['date_last_email'],
            'required_values' => ['date_last_email']]
    );

    Check\check_datetime(
        'date_last_email',
        $train['email_attributes']['date_last_email'],
        ['check' => 'datetime',
            'nullable' => false,
            'format' => 'Y-m-d H:i:s',
            'session' => false]
    );

    if (\array_key_exists('email_sent', $train['email_attributes'])
        && $train['email_attributes']['email_sent'] === true) {
        $train['email_attributes']['date_last_email'] = \gmdate('Y-m-d H:i:s', \time());
    }

    return $train;
}
