<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages;

/**
 * Responds with the error from the last JSON operation, or returns true.
 *
 * @return bool true|false    Returns true if there are no errors. Responds if there are.
 */
function check_json_error(): bool
{
    static $json_error_list = [
        JSON_ERROR_NONE => '',
        JSON_ERROR_DEPTH => 'JSON_ERROR_DEPTH: Maximum stack depth exceeded',
        JSON_ERROR_STATE_MISMATCH => 'JSON_ERROR_STATE_MISMATCH: Underflow or the modes mismatch',
        JSON_ERROR_CTRL_CHAR => 'JSON_ERROR_CTRL_CHAR: Unexpected control character found',
        JSON_ERROR_SYNTAX => 'JSON_ERROR_SYNTAX: Syntax error, malformed JSON',
        JSON_ERROR_UTF8 => 'JSON_ERROR_UTF8: Malformed UTF-8 characters',

        JSON_ERROR_RECURSION => 'JSON_ERROR_RECURSION: Recursive references cannot be encoded',
        JSON_ERROR_INF_OR_NAN => 'JSON_ERROR_INF_OR_NAN: Includes NAN or INF',
        JSON_ERROR_UNSUPPORTED_TYPE => 'JSON_ERROR_UNSUPPORTED_TYPE: Bad type like a resource given',
        JSON_ERROR_INVALID_PROPERTY_NAME => 'JSON_ERROR_INVALID_PROPERTY_NAME: A \u0000 key character',
        JSON_ERROR_UTF16 => 'JSON_ERROR_UTF16: Single unpaired UTF-16 surrogate escape'
    ];

    $error_key = \json_last_error();

    if ($error_key !== 0) {
        $json_error = $json_error_list[$error_key] ?? "Unknown JSON Error [{$error_key}]";

        Messages\respond(500, [$json_error]);
    }

    return true;
}
