<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * This will convert a JSON format into array.
 *
 * @param mixed $json The JSON Format to decode
 *
 * @return array returns the decoded JSON as an array
 */
function decode_json($json): array
{
    $array = [];

    if (!\is_string($json)) {
        Messages\respond(501, ['The JSON input is not a string']);
    }

    if ($json !== '') {
        $json = \trim($json);

        $json = \html_entity_decode($json);

        // Remove BOM characters
        $json = \str_replace("\xEF\xBB\xBF", '', $json);

        $json = \rtrim($json, "\0");

        $json = \mb_convert_encoding($json, 'UTF-8');

        // Convert the JSON into an array
        $array = (array) \json_decode($json, true);

        CrudJson\check_json_error();
    }

    return $array;
}
