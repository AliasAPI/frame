<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;

/**
 * Encodes the JSON format using utf-8 encoding format.
 *
 * @param mixed  $array  The array that will be encoded to JSON
 * @param string $pretty Set a string to make the JSON more readable
 *                       The default is false to reduce characters
 *
 * @return string Returns the encoded JSON format
 */
function encode_json($array, $pretty = ''): string
{
    if ($pretty !== '') {
        // Display formatted JSON (in files) that is easier to read
        $json = \json_encode(
            $array,
            JSON_PRETTY_PRINT |
            JSON_UNESCAPED_UNICODE |
            JSON_UNESCAPED_SLASHES |
            JSON_NUMERIC_CHECK
        );
    } else {
        $json = \json_encode(
            $array,
            JSON_UNESCAPED_UNICODE |
            JSON_UNESCAPED_SLASHES |
            JSON_NUMERIC_CHECK
        );
        // Remove all the extra lines and spaces to reduce output
        $json = \preg_replace('/\\n/', '', $json);

        $json = \preg_replace('!\s+!', ' ', $json);
    }

    CrudJson\check_json_error();

    return \mb_convert_encoding($json, 'UTF-8');
}
