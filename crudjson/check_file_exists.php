<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToRetrieveMetadata;

/**
 * Check if the file exists in the provided directory
 * Warning: The Flysystem will not return true for directories that exist.
 *
 * @param string $directory The directory the file is located in
 * @param string $filename  The filename
 *
 * @return bool Returns TRUE if file exists. FALSE if not.
 */
function check_file_exists(string $directory, string $filename): bool
{
    $exists = false;

    try {
        $adapter = new LocalFilesystemAdapter($directory);

        $filesystem = new Filesystem($adapter);

        $exists = $filesystem->fileExists($filename);
    } catch (FilesystemException|UnableToRetrieveMetadata $exception) {
        Messages\respond(500, ["Unable to check if [{$filename}] file exists."]);
    }

    return $exists;
}
