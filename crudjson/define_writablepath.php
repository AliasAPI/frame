<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages;

/**
 * Define the default writable path for the JSON and debugging files.
 *
 * @return array $train  Returns the writable_path in train settings
 */
function define_writablepath(array $train, bool $mandate = false): array
{
    $writable_path = '';
    $default_path = '';

    if (isset($train['settings']['writable_path'])) {
        $path = $train['setting']['writable_path'];
    } else {
        $path = '';
    }

    if (\defined('WRITABLEPATH')) {
        $default_path = WRITABLEPATH;
    } elseif (\defined('DOCROOT')
              && $path === '') {
        $default_path = '/data/files';
    } elseif ($path === '') {
        $default_path = '/data/files';
    }

    if ($mandate === true
        && $path !== ''
        && !\is_writable($path)) {
        Messages\respond(501, ['The specified writable_path is not writable.']);
    } elseif ($mandate === true
              && $path === ''
              && !\is_writable($default_path)) {
        Messages\respond(501, ["The default writable_path ({$default_path}) is not writable."]);
    } elseif ($path !== ''
              && \is_writable($path)) {
        // Use the specified path as the datafiles folder
        $writable_path = $path;
    } elseif ($path === ''
              && \is_writable($default_path)) {
        $writable_path = $default_path;
    } elseif (\is_writable(\ini_get('upload_tmp_dir'))) {
        // Use the lampp (/opt/lampp/temp/) directory
        $writable_path = \ini_get('upload_tmp_dir');
    } elseif (\is_writable(\sys_get_temp_dir())) {
        // Use the /tmp directory
        $writable_path = \sys_get_temp_dir();
    } else {
        Messages\respond(501, ['The server user cannot write to the filesystem.']);
    }

    if ($writable_path !== '') {
        \defined('WRITABLEPATH') || \define('WRITABLEPATH', $writable_path);

        $train['settings']['writable_path'] = $writable_path;
    }

    return $train;
}
