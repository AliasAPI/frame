<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\Messages;

/**
 * Defines the path below DOCROOT as a CONSTANT.
 * Set the config/ directory to 0700.
 *
 * @param string $name      The directory name to set as a constant
 * @param string $directory A DOCROOT subdirectory
 * @param array  $train     permissions, recursive
 *
 * @return array $train      Returns the train with the defined directory
 */
function define_directory(string $name, string $directory, array $train): array
{
    if (!\defined('DOCROOT')) {
        Messages\respond(501, ['The DOCROOT has not been defined.']);
    }

    $path = \realpath(DOCROOT . $directory);

    if ($path) {
        $constant = \mb_strtoupper($name);

        \defined($constant) || \define($constant, $path . DIRECTORY_SEPARATOR);

        $train['settings'][$constant] = $path;
    }

    return $train;
}
