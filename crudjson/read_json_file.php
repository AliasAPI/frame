<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToReadFile;

/**
 * This will read the value inside the JSON file from the specified path.
 *
 * @param string $writable_path The path for the JSON file
 * @param string $filename      The name of the JSON file
 *
 * @return array Returns the value inside the JSON file as an array
 */
function read_json_file(string $writable_path, string $filename): array
{
    $file_array = [];

    if ($writable_path === ''
        && \defined('WRITABLEPATH')) {
        $writable_path = WRITABLEPATH;
    }

    $exists = CrudJson\check_file_exists($writable_path, $filename);

    if (!$exists) {
        return $file_array;
    }

    try {
        $adapter = new LocalFilesystemAdapter($writable_path);

        $filesystem = new Filesystem($adapter);

        $json = $filesystem->read($filename);

        $file_array = CrudJson\decode_json($json);
    } catch (FilesystemException|UnableToReadFile $exception) {
        Messages\respond(500, ["rjf: {$exception}"]);
    }

    return $file_array;
}
