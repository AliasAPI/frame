<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToDeleteFile;

/**
 * Deletes the JSON File from the specified path.
 *
 * @param string $writable_path the path of the JSON file
 * @param string $filename      the name of the JSON file
 *
 * @return bool Returns TRUE if file is deleted. False if not
 */
function delete_json_file(string $writable_path, string $filename): bool
{
    $deleted = false;

    try {
        $adapter = new LocalFilesystemAdapter($writable_path);

        $filesystem = new Filesystem($adapter);

        $filesystem->delete($filename);

        $exists = CrudJson\check_file_exists($writable_path, $filename);

        $deleted = ($exists === true) ? false : true;
    } catch (FilesystemException|UnableToDeleteFile $exception) {
        Messages\respond(500, ["Unable to delete [{$filename}] file."]);
    }

    return $deleted;
}
