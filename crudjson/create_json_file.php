<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToWriteFile;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;

/**
 * This will create a JSON file on the specified location path.
 * https://flysystem.thephpleague.com/v2/docs/adapter/local/.
 *
 * @param string $writable_path the path of the file
 * @param string $filename      the file name
 * @param array  $data          the JSON data to be added
 *
 * @return bool Return TRUE if JSON file has been created. FALSE if not.
 */
function create_json_file($writable_path, $filename, $data): bool
{
    $created = false;

    $created = CrudJson\check_file_exists($writable_path, $filename);

    if ($created === true) {
        return true;
    }

    try {
        $adapter = new LocalFilesystemAdapter(
            $writable_path,
            PortableVisibilityConverter::fromArray([
                'file' => [
                    'public' => 0640,
                    'private' => 0604,
                ],
                'dir' => [
                    'public' => 0740,
                    'private' => 7604,
                ],
            ]),
            // Write flags
            LOCK_EX,
            // How to deal with links, either DISALLOW_LINKS or SKIP_LINKS
            // Disallowing them causes exceptions when encountered
            LocalFilesystemAdapter::DISALLOW_LINKS
        );

        $filesystem = new Filesystem($adapter);

        $json = CrudJson\encode_json($data, 'pretty');

        $config = [];

        $filesystem->write($filename, $json);

        $created = CrudJson\check_file_exists($writable_path, $filename);
    } catch (FilesystemException|UnableToWriteFile $exception) {
        Messages\respond(500, ["Unable to create the [{$filename}] file."]);
    }

    return $created;
}
