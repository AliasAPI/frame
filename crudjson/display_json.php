<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

/**
 * Displays the JSON format.
 *
 * @param string $json the JSON format you want to display
 *
 * @return string retruns the JSON format
 */
function display_json($json): string
{
    // This may already be done in the set_initial_settings();
    if (!headers_sent()) {
        header('Content-Type: application/json; charset=UTF-8');
    }

    // Display the json
    echo $json;

    // Don't allow any more processing so the displayed json is valid.
    exit;
}
