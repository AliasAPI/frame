<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;

/**
 * This will make the JSON easier to read for humans.
 *
 * @param array $json The JSON format
 *
 * @return mixed Returns the newly formatted JSON
 */
function humanize_json($json): mixed
{
    $array = CrudJson\decode_json($json);

    // Format the json with pretty line returns
    $new_json = CrudJson\encode_json($array, 'pretty');

    // Strip out the ugly characters
    $new_json = preg_replace("/\\ \\[\r/", "\r", $new_json);
    $new_json = preg_replace('/  ]\\ /', "\r", $new_json);
    $new_json = str_replace('{', '', $new_json);
    $new_json = str_replace('},', '', $new_json);
    $new_json = str_replace('}', '', $new_json);
    $new_json = str_replace('"', '', $new_json);
    $new_json = str_replace(',', '', $new_json);
    $new_json = stripslashes($new_json);
    // Added 03-05-19
    $new_json = preg_replace("/\\[\n/", "\r", $new_json);
    $new_json = preg_replace("/]\n/", "\r", $new_json);

    return preg_replace('/]/', "\r", $new_json);
}
