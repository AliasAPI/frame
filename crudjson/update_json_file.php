<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * Updates the JSON file from the specified path.
 *
 * @param string $path     the path of the JSON file
 * @param string $filename the name of the JSON file
 * @param array  $data     the data to be added for the update
 *
 * @return bool Returns TRUE if the JSON file has been updated. FALSE if not.
 */
function update_json_file(string $path, string $filename, array $data, bool $overwrite): bool
{
    $before_array = [];
    $created = false;

    if (\count($data) === 0) {
        Messages\respond(501, ["There is no new data for {$filename}"]);
    }

    // Check to see if the file exists and if it does not, then create it.
    $created = CrudJson\check_file_exists($path, $filename);

    if ($created === true) {
        $before_array = CrudJson\read_json_file($path, $filename);
    }

    if ($overwrite === false) {
        foreach ($before_array as $key => $value) {
            if (\array_key_exists($key, $data)) {
                unset($data[$key]);
            }
        }
    }

    if (\count($data) > 0) {
        $deleted = CrudJson\delete_json_file($path, $filename);

        if ($deleted === true) {
            $after_array = \array_merge_recursive($before_array, $data);

            \ksort($after_array);

            $created = CrudJson\create_json_file($path, $filename, $after_array);
        }
    }

    return $created;
}
