<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;

/**
 * Deletes expired JSON tag files from the WRITABLEPATH.
 *
 * @return void returns nothing
 */
function delete_tag_files(int $minutes): void
{
    if (\defined('WRITABLEPATH')) {
        $deadline = \date('YmdHis', \strtotime("-{$minutes} minutes"));

        $expired = WRITABLEPATH . 'TAG' . $deadline;

        $tag_files = \glob(WRITABLEPATH . 'TAG*');

        foreach ($tag_files as $filename) {
            if ($filename <= $expired) {
                $delete_file = \str_replace(WRITABLEPATH, '', $filename);

                CrudJson\delete_json_file(WRITABLEPATH, $delete_file);
            }
        }
    }
}
