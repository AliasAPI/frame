<?php

declare(strict_types=1);

namespace AliasAPI\CrudJson;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

/**
 * This will create or update the JSON file at the specified location path.
 *
 * @param string $filename the file name
 * @param array  $data     the JSON data to be added
 *
 * @return bool Return TRUE if JSON file has been created. FALSE if not.
 */
function setup_config_file(array $request, string $filename, array $data): bool
{
    $updated = false;

    if (!\array_key_exists('action', $request)
        || $request['action'] !== 'setup services') {
        return $updated;
    }

    if (!\defined('CONFIGPATH')) {
        Messages\respond(501, ['The config path has not been defined.']);
    }

    return CrudJson\update_json_file(CONFIGPATH, $filename, $data, false);
}
