<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload;

/**
 * Receives a request from the Client and returns a response in a procedure.
 *
 * @param mixed[] $train
 *
 * @return array $train
 */
function track(array $train): array
{
    Autoload\require_autoload_files();

    $train = Autoload\define_docroot($train);

    $train = Autoload\configure_autoload_paths($train);

    $train = Autoload\configure_file_load_types($train);

    $train = Autoload\configure_recursive_depth($train);

    $load_files = Autoload\glob_autoload_paths_files($train);

    $extra_files = Autoload\configure_extra_autoload_files($train);

    Autoload\autoload_specified_files($load_files, $extra_files);

    return $train;
}
