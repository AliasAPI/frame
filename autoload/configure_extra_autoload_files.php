<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * Configures the extra files that will be loaded by the autoloader.
 *
 * @return array $extra_files     Returns the extra files to autoload
 */
function configure_extra_autoload_files(array $train): array
{
    $default_files = [];

    if (!isset($train['settings']['extra_files'])
        || !\is_array($train['settings']['extra_files'])
        || \count($train['settings']['extra_files']) === 0) {
        $train['settings']['extra_files'] = [];
    }

    if (\defined('DOCROOT')
        && \file_exists(DOCROOT . 'vendor/autoload.php')) {
        // Run the composer autoloader
        $default_files[] = DOCROOT . 'vendor/autoload.php';
    }

    return \array_merge($default_files, $train['settings']['extra_files']);
}
