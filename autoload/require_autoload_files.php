<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * This will output all of the autoload files.
 *
 * @return void returns the autoload files
 */
function require_autoload_files(): void
{
    $autoload_files = glob(__DIR__ . DIRECTORY_SEPARATOR . '*.php');

    foreach ($autoload_files as $file) {
        /** @psalm-suppress UnresolvableInclude */
        require_once $file;
    }
}
