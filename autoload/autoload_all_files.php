<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload;

/**
 * Quick autoloader using defaults for development purposes.
 */
function autoload_all_files(): void
{
    require_once __DIR__ . '/require_autoload_files.php';

    require_once __DIR__ . '/track.php';

    Autoload\track([]);
}
