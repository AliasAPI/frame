<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload;

/**
 * Creates a list of all the files to load.
 *
 * @return array $all_files   returns all of the files to be autoloaded
 */
function glob_autoload_paths_files(array $train): array
{
    $all_files = [];
    $files_glob = [];

    foreach ($train['settings']['load_paths'] as $path) {
        foreach ($train['settings']['file_load_types'] as $pattern) {
            $files_glob = Autoload\glob_files_recursively(
                $path,
                $path,
                $pattern,
                $train['settings']['recursive_depth']
            );
            $all_files = \array_merge($all_files, $files_glob);
        }
    }

    return $all_files;
}
