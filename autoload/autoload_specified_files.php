<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * Autoload all of the specified load_files and extra_files.
 *
 * @param array $load_files  The path of default files
 * @param array $extra_files The path of extra files
 */
function autoload_specified_files(array $load_files, array $extra_files): void
{
    $load_these_files = \array_merge($load_files, $extra_files);

    if (\count($load_these_files) < 1) {
        exit('["There are no files to autoload. Check the path variables."]');
    }

    foreach ($load_these_files as $index => $file) {
        // if (! \is_dir($file) && \file_exists($file)) {
        if (\is_file($file)) {
            require_once $file;
        }
    }
}
