<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

use AliasAPI\Autoload;

/**
 * This will glob the files recursively.
 *
 * @param string $start_path      the base path
 * @param string $path            the complete path
 * @param string $pattern         the pattern to be used
 * @param int    $recursive_depth the max depth for recursion
 *
 * @return array returns the files
 */
function glob_files_recursively(string $start_path, string $path = '', string $pattern = '*', int $recursive_depth = 3): array
{
    $paths = \glob($path . '*', GLOB_MARK | GLOB_ONLYDIR);

    $files = \glob($path . $pattern);

    foreach ($paths as $path) {
        $path_depth = Autoload\calculate_path_depth($start_path, $path);

        if ($path_depth <= $recursive_depth) {
            $files = array_merge(
                $files,
                Autoload\glob_files_recursively(
                    $start_path,
                    $path,
                    $pattern,
                    $recursive_depth
                )
            );
        }
    }

    return $files;
}
