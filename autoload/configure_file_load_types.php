<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * This will configure on what filetypes to load, which is only php.
 *
 * @return array $train
 */
function configure_file_load_types(array $train): array
{
    if (!isset($train['settings']['file_load_types'])
        || !is_array($train['settings']['file_load_types'])
        || \count($train['settings']['file_load_types']) === 0) {
        $train['settings']['file_load_types'] = ['*.php'];
    }

    return $train;
}
