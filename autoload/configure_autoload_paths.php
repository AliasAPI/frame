<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * Creates the path for the files to autoload.
 *
 * @return array $train
 */
function configure_autoload_paths(array $train): array
{
    if (\defined('DOCROOT')
        && !isset($train['settings']['load_paths'])
        || empty($train['settings']['load_paths'])) {
        if (\file_exists(DOCROOT . 'src')) {
            // Files can be in src/functions because files are globbed recursively.
            $train['settings']['load_paths'][] = DOCROOT . 'src';
        }

        if (\file_exists(DOCROOT . 'vendor/aliasapi')) {
            $train['settings']['load_paths'][] = DOCROOT . 'vendor/aliasapi';
        }
    }

    return $train;
}
