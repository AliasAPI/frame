<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * Configure the max depth of directory recursion.
 *
 * @return array $train
 */
function configure_recursive_depth(array $train): array
{
    if (!isset($train['settings']['recursive_depth'])
        || !\is_int($train['settings']['recursive_depth'])) {
        $train['settings']['recursive_depth'] = 3;
    }

    return $train;
}
