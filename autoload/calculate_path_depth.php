<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * Calculates how many characters is the path of the file.
 *
 * @param string $base_path the main directory
 * @param string $path      the full path of the file location
 *
 * @return int returns the path depth as an integer
 */
function calculate_path_depth(string $base_path, string $path): int
{
    // Subtract the base_path to easily count the subdirectories
    $sub_path = \str_replace($base_path, '', $path);

    // Add the +1 to start the count at 1 rather than 0 for better understanding
    return \count(\array_filter(\explode('/', $sub_path))) + 1;
}
