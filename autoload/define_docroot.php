<?php

declare(strict_types=1);

namespace AliasAPI\Autoload;

/**
 * This will define the DOCROOT of the project.
 *
 * @return array $train
 */
function define_docroot(array $train): array
{
    // AliasAPI is a composer package that should be located in vendor/
    // ex: /microservice_directory/vendor/aliasapi/frame/
    $omit = '/vendor/aliasapi/frame/autoload';

    $docroot = \realpath(\str_ireplace($omit, '', __DIR__)) . DIRECTORY_SEPARATOR;

    if (!\defined('DOCROOT')) {
        \define('DOCROOT', $docroot);
    }

    $train['settings']['docroot'] = $docroot;

    return $train;
}
