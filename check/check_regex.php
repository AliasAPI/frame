<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes, reformats, and validates an input string (that might be nullable)
 * Make sure the regex is strict so that it will provide sanitization of the value
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);.
 *
 * @param string $name    The variable name of the string
 * @param mixed  $value   The text string to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_regex($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    if ($value === null) {
        Messages\set_reply(400, ["The [{$name}] value is null."]);

        return false;
    }

    if (!\preg_match($options['regex'], $value, $matches)) {
        Messages\set_reply(400, ["Please enter a valid value for [{$name}]."]);

        return false;
    }

    Check\set_key_value($name, $matches[0], 'string');

    return true;
}
