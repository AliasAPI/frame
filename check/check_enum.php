<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure a value matches one of the acceptable values.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the enum
 * @param string $value   The text value to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_enum($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    if (\count($options['options']) > 0
        && !\in_array($value, $options['options'], true)) {
        $error = "{$name} = [{$value}] and it should be one of the following: [";

        // List all of the value choices in the error message
        foreach ($options['options'] as $acceptable_value) {
            $error .= "{$acceptable_value}, ";
        }

        $error .= ']';

        Messages\set_reply(400, ["{$error}"]);

        return false;
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
