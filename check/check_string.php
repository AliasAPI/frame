<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks the attributes of a string
 * Get the sanitized, and validated value from Check\get_key_value($name);.
 *
 * @param string $name    The variable name of the string
 * @param mixed  $value   The text string to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_string($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    $value = \filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);

    if (\mb_strlen($value) < $options['range']['min']) {
        Messages\set_reply(401, ["Please enter a [{$name}] that is " .
                                 'at least [' . $options['range']['min'] . '] characters long.']);

        return false;
    }

    if (\mb_strlen($value) > $options['range']['max']) {
        Messages\set_reply(401, ["Please enter a [{$name}] that is " .
                                 'less than [' . $options['range']['max'] . '] characters long.']);

        return false;
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
