<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure the variable value is a float within a range.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 * https://github.com/auraphp/Aura.Filter/blob/2.x/src/Rule/Sanitize/Integer.php
 *
 * @param string $name    The variable name of the integer
 * @param mixed  $value   The text integer to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_integer($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // Cast the value to a string for the following functions
    $value = (string) $value;

    // Remove all non-numeric characters except the minus sign
    $value = \preg_replace('/[^0-9-]/', '', $value);

    // Remember if the number is leading negative sign
    $negative = \preg_match('/^-/', $value);

    // Remove negative signs between numbers
    $value = \str_replace('-', '', $value);

    // Replace the leading negative sign
    if ($negative) {
        $value = '-' . $value;
    }

    // Double-cast here to honor scientific notation
    // (int) 1E5 == 15, but (int) (float) 1E5 == 100000
    $value = (float) $value;
    $value = (int) $value;

    if ($value < $options['range']['min']) {
        Messages\set_reply(400, ["The [{$value}] is below the [" . $options['range']['min'] . '] mimimum.']);

        return false;
    }

    if ($value > $options['range']['max']) {
        Messages\set_reply(400, ["The [{$value}] is above the [" . $options['range']['max'] . '] maximum.']);

        return false;
    }

    Check\set_key_value($name, $value, 'integer');

    return true;
}
