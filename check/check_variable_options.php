<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Messages;

/**
 * Checks the variable configuration settings (one time).
 *
 * @param string $name    The variable name of the variable to be checked
 * @param array  $options The settings to use while checking the variable
 *
 * @return bool true        Returns true if the value is good
 */
function check_variable_options($name, $options): bool
{
    if (\count($options) === 0) {
        Messages\respond(500, ['Please configure the variables with configure_variables().']);
    }

    if (!\array_key_exists('check', $options)
        || $options['check'] === '') {
        Messages\respond(501, ["The check option for [{$name}] is not configured yet."]);
    }

    if (!\array_key_exists('nullable', $options)
        || !\is_bool($options['nullable'])) {
        Messages\respond(501, ["The nullable option for [{$name}] is not set correctly."]);
    }

    // Require values to not be null by default
    if (!\array_key_exists('nullable', $options)) {
        $options['nullable'] = false;
    }

    // Variables that use a numeric minimum to maximum range
    if (\in_array($options['check'], ['float', 'integer', 'name', 'string'], true)) {
        if (!\array_key_exists('range', $options)
            || \count($options['range']) === 0) {
            Messages\respond(500, ["The options range for [{$name}] is not set correctly."]);
        }

        if (!\array_key_exists('min', $options['range'])) {
            Messages\respond(500, ["The options range min set for [{$name}] is invalid."]);
        }

        if (!\is_int($options['range']['min'])) {
            Messages\respond(500, ["The [{$name}] options range min is not an integer."]);
        }

        if (!\array_key_exists('max', $options['range'])) {
            Messages\respond(500, ["The options range max set for [{$name}] is invalid."]);
        }

        if (!\is_int($options['range']['max'])) {
            Messages\respond(500, ["The [{$name}] options range max is not an integer."]);
        }
    }

    // Variables that use a datetime format
    if (\in_array($options['check'], ['date', 'datetime', 'time'], true)) {
        if (!\array_key_exists('format', $options)
            || $options['format'] === '') {
            Messages\respond(500, ["The format for [{$name}] is not configured."]);
        }
    }

    // enum
    if ($options['check'] === 'enum'
        && (!\array_key_exists('options', $options)
        || \count($options['options']) === 0)) {
        Messages\respond(500, ["The enum option for [{$name}] is not set yet."]);
    }

    // regex
    if ($options['check'] === 'regex'
        && (!\array_key_exists('regex', $options)
        || $options['regex'] === '')) {
        Messages\respond(500, ["The regex option for [{$name}] is not set yet."]);
    }

    // array
    if ($options['check'] === 'array') {
        if (!\array_key_exists('filter_values', $options)) {
            Messages\respond(500, ["The filter_values option for [{$name}] is not set yet."]);
        }

        if (!\array_key_exists('required_keys', $options)) {
            Messages\respond(500, ["The required_keys option for [{$name}] is not set yet."]);
        }

        if (!\array_key_exists('required_values', $options)) {
            Messages\respond(500, ["The required_values option for [{$name}] is not set yet."]);
        }
    }

    return true;
}
