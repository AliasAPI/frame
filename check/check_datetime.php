<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes, (re)formats, and validates a datetime string (that might be null).
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the datetime
 * @param mixed  $value   The datetime string to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_datetime($name, $value, $options = []): bool
{
    $err = [];
    $err['warnings'] = [];
    $err['errors'] = [];

    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // Returns DateTime Object([date]=>2021-12-24 00:17:41.000000 [timezone_type]=>3 [timezone] => UTC)
    if (!\is_string($value)) {
        Messages\set_reply(400, ["The [{$name}] datetime is not a string."]);

        return false;
    }

    $new_value = \date_create_immutable_from_format($options['format'], $value);

    $new_value = \date_format($new_value, $options['format']);

    if ($value !== $new_value) {
        Messages\respond(500, ["The datetimes does not match {$new_value}, {$value}"]);
    }

    $err = \date_get_last_errors();

    if ((isset($err['warnings']) 
         && \count($err['warnings']) > 0)
         || (isset($err['errors'])
             && \count($err['errors']) > 0)) {
        $errors = \implode(', ', \array_merge($err['warnings'], $err['errors']));
        
        Messages\set_reply(500, ["[{$name}] " . $errors]);

        return false;
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
