<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure the input value is a phone number
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 * Only matches U.S. phone numbers.
 *
 * @param string $name    The variable name of the string
 * @param string $value   The text string to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_phone($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // // all on one line...
    // $regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/';

    // // or broken up
    // $regex = '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
    //         .'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
    //         .'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/';
    // https://www.php.net/manual/en/function.preg-match.php

    $value = \str_replace(' ', '', $value);
    $value = \str_replace('(', '', $value);
    $value = \str_replace(')', '', $value);
    $value = \str_replace('.', '-', $value);

    $numbers = \str_replace('-', '', $value);

    if ((\mb_strlen($numbers) > 1
        && \mb_strlen($numbers) < 10)
        || \mb_strlen($numbers) > 14
        || !\filter_var($value, FILTER_SANITIZE_NUMBER_INT)) {
        Messages\respond(400, ['Please enter a valid mobile phone number.']);
    }

    // Normalize the phone numbers to XXX-XXX-XXXX
    $value = \preg_replace('/^1?(\\d{3})(\\d{3})(\\d{4})$/', '$1-$2-$3', $numbers);

    if (\mb_strlen($value) > 1
        && !\preg_match(
            '/^1?\s?\(\d{3}\)\s?\d{3}[-\s]?\d{4}$|^1?\s?\d{3}[-\s]?\d{3}[-\s]?\d{4}$/',
            $value
        )) {
        Messages\respond(400, ['Please enter a valid mobile phone number.']);
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
