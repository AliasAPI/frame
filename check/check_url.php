<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure a URL if formatted correctly and pings it.
 *
 * @param string $name    The variable name of the URL
 * @param string $value   The URL to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the URL is good
 */
function check_url($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    if (\mb_strpos($value, 'http') !== 0) {
        Messages\set_reply(403, ["The [{$name}] URL does not start with http(s)."]);

        return false;
    }

    $original_value = \filter_var($value, FILTER_VALIDATE_URL);

    $value = \filter_var($value, FILTER_SANITIZE_URL);

    if ($original_value !== $value) {
        Messages\set_reply(403, ["The [{$name}] URL is not valid."]);

        return false;
    }

    // Use the following regex instead of  \filter_var($sanitized_url, FILTER_VALIDATE_URL)
    // See https://mathiasbynens.be/demo/url-regex

    // $regex = '_^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$_iuS';

    // if (! \preg_match($regex, $value, $matches)) {
    //     Messages\set_reply(403, ["The [$name] URL [$value] is not valid."]);

    //     return false;
    // } else {
    //     $value = $matches[0];
    // }

    Check\set_key_value($name, $value, 'string');

    return true;
}
