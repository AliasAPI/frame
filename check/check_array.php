<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure required_keys and required_values are in a flat array.
 *
 * @param string $name    The variable name of the array
 * @param array  $array   The flat array to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the array is good
 */
function check_array($name, $array, $options = []): bool
{
    $errors = 0;
    $missing_keys = [];
    $missing_values = [];

    $options = Check\set_variable_options($name, $options);

    if (\count($array) < 1) {
        Messages\set_reply(501, ["The [{$name}] array is currently empty."]);

        ++$errors;
    }

    if (\count($options['required_keys']) === 0
        && \count($options['required_values']) === 0) {
        Messages\set_reply(501, ["The [{$name}] array required_keys and required_values are not configured."]);

        ++$errors;
    }

    if (\count($options['required_keys']) > 0) {
        foreach ($options['required_keys'] as $index => $key) {
            if (!\array_key_exists($key, $array)) {
                $missing_keys[] = $key;
            }
        }
    }

    if (\count($options['required_values']) > 0) {
        foreach ($options['required_values'] as $index => $key) {
            if ((\array_key_exists($key, $array)
                && !\is_scalar($array[$key]))

                && (\is_array($array[$key])
                && \count($array[$key]) === 0)) {
                $missing_values[] = $key;
            }
        }
    }

    if (\count($missing_keys) > 0) {
        $missed_keys = '';

        foreach ($missing_keys as $index => $missed_key) {
            $missed_keys .= $missed_key . ', ';
        }

        $missed_keys = \rtrim($missed_keys, ', ');

        Messages\set_reply(501, ["The [{$name}] array is missing the [{$missed_keys}] key(s)."]);

        ++$errors;
    }

    if (\count($missing_values) > 0) {
        $missed_values = '';

        foreach ($missing_values as $index => $missed_value) {
            $missed_values .= $missed_value . ', ';
        }

        $missed_values = \rtrim($missed_values, ', ');

        Messages\set_reply(501, ["The [{$name}] array is missing the [{$missed_values}] value(s)."]);

        ++$errors;
    }

    if ($options['filter_values'] === true) {
        foreach ($array as $key => $value) {
            if (\is_string($value)) {
                // Remove the encoding for the regex and other checks
                $value = \trim(\htmlspecialchars_decode($value, ENT_HTML5));

                $array[$key] = \filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
            } elseif (\is_float($value)) {
                $array[$key] = \filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);
            } elseif (\is_int($value)) {
                $array[$key] = \filter_var($value, FILTER_SANITIZE_NUMBER_INT);
            } elseif (\is_bool($value)) {
                $array[$key] = $value;
            } elseif ($value === null) {
                $array[$key] = $value;
            } else {
                $type = \gettype($value);

                Messages\set_reply(500, ["The [{$key}] value is [{$type}], not scalar."]);

                $missed = ($missed !== true) ? true : false;

                ++$errors;
            }
        }

        // Only save arrays that have been sanitized
        Check\set_key_value($name, $array, 'array');
    }

    // Return false if there were errors
    return ($errors > 0) ? false : true;
}
