<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Check the configuration used for validating each (custom) variable.
 *
 * @param string $name    The variable name of the variable to be checked
 * @param array  $options The settings to use while checking the variable
 *
 * @return array $options  The options are returned to the Check\ functions
 */
function set_variable_options($name, $options = []): array
{
    if ($name === '') {
        Messages\respond(501, ['The name for set variable options is not set.']);
    }

    // If the options are not provided above . . .
    if (\count($options) === 0) {
        // . . . use the pre-configured variables
        $variables = Check\get_key_value('variables');

        if (!\array_key_exists($name, $variables)) {
            Messages\respond(501, ["The [{$name}] variable is not configured yet."]);
        }

        return $variables[$name];
    }

    if ($name === 'variables') {
        // Call Check\set_variable_options() initially to
        // check each of the variable configurations once
        foreach ($options as $name => $settings) {
            Check\check_variable_options($name, $settings);
        }

        // Set the variables in Check\configure_variables()
        Check\set_key_value('variables', $options, 'array');

        return $options;
    }

    // If a custom variable is configured above
    // stop processing if the options are wrong
    Check\check_variable_options($name, $options);

    return $options;
}
