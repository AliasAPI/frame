<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes, (re)formats, and validates a password string (that cannot be null)
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);.
 *
 * @param string $name    The variable name of the password
 * @param string $value   The password text to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_password($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // “123456”, “password” and “qwerty”
    // identity and access management regulations, such as SOX for financial services and HIPAA for healthcare transactions.
    // if (\strpos($train['users']['new_password_1'], 'password') !== false) {
    //     Messages\set_reply(403, ["Please do not include the word password in the password."]);
    // }

    // if (\array_key_exists('username', $train['user'])
    //     && \strpos($train['users']['new_password_1'], $train['user']['username']) !== false) {
    //     Messages\set_reply(403, ["Please do not include the username in the password."]);
    // }

    // elseif ($type === 'password') {
    //     $match = '/^(?=.* [a-zA-Z ]||d)(? =. * [a-zA-Z ] | \W + ) ( ?!. * \s).*$/ ' ;
    //     $error = 'can consist of English letters, numbers and special characters. Only numbers are prohibited. ' ;

    // } elseif ($type === 'password-hard') {
    //     $match = '/ ^ (? =^. * $) ( (? = . * \d) | (? = . * \W+))(?! ) (? =. * [az]). * $ / ' ;
    //     $error = 'must contain at least one digit, one uppercase and one lowercase English letter and one special character.' ;

    // }

    Check\set_key_value($name, $value, 'string');

    return true;
}
