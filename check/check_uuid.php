<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes and validates the format of  a uuid string (that might be nullable)
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);.
 *
 * @param string $name    The variable name of the uuid
 * @param mixed  $value   The text uuid to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_uuid($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    $uuid_regex = '/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
                  '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i';

    if (!\preg_match($uuid_regex, $value, $matches)) {
        Messages\set_reply(500, ["The [{$name}] is not valid."]);

        return false;
    }

    Check\set_key_value($name, $matches[0], 'string');

    return true;
}
