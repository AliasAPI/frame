<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Messages;

/**
 * Returns the value set based on a key in Check\set_key_value().
 *
 * @param string $key The index key or name of the variable
 *
 * @return mixed $value  The value after it has been validated
 */
function get_key_value($key): mixed
{
    if (!\array_key_exists('only_set_in_check_set_key_value', $GLOBALS)) {
        Messages\respond(501, ['The key value array has not been set yet.']);

        return null;
    }

    if (!\array_key_exists($key, $GLOBALS['only_set_in_check_set_key_value'])) {
        Messages\respond(501, ["The value has not been set for [{$key}] yet."]);

        return null;
    }

    return $GLOBALS['only_set_in_check_set_key_value'][$key];
}
