<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes, edits, and validates an email address.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the email
 * @param string $value   The email text to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_email($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    // https://www.regular-expressions.info/email.html

    // Remove additional characters allowed by FILTER_SANITIZE_EMAIL
    // $value = \str_replace("<", "", $value);
    // $value = \str_replace(">", "", $value);
    // $value = \str_replace(",", "", $value);
    $value = \str_replace('[', '', $value);
    $value = \str_replace(']', '', $value);
    $value = \str_replace('\'', '', $value);
    // $value = \str_replace("\"", "", $value);
    // $value = \str_replace(" ", "", $value);
    // $value = \str_replace(";", "", $value);

    // $value = \trim($value);
    $value = \mb_strtolower($value);

    // Remove common user error inputs
    $value = \str_replace('mailto:', '', $value);
    $value = \str_replace('http://', '', $value);
    $value = \str_replace('http:/', '', $value);

    // Replace common user error trucations
    $value = \preg_replace('/\/$/', '', $value);
    $value = \preg_replace('/\.con$/', '.com', $value);
    $value = \preg_replace('/\.ed$/', '.edu', $value);
    $value = \preg_replace('/\.go$/', '.gov', $value);
    $value = \preg_replace('/\.ibm$/', '.ibm.com', $value);
    $value = \preg_replace('/\.mi$/', '.mil', $value);
    $value = \preg_replace('/\.ocm$/', '.com', $value);
    $value = \preg_replace('/us\.army$/', 'us.army.mil', $value);
    $value = \preg_replace('/\.rr$/', '.rr.com', $value);

    // If it ENDS in an extention with an extra character, trim off an excess character
    $value = \preg_replace('/\.biz.$/', '.biz', $value);
    $value = \preg_replace('/\.com.$/', '.com', $value);
    $value = \preg_replace('/\.edu.$/', '.edu', $value);
    $value = \preg_replace('/\.gov.$/', '.gov', $value);
    $value = \preg_replace('/\.mil.$/', '.mil', $value);
    $value = \preg_replace('/\.net.$/', '.net', $value);
    $value = \preg_replace('/\.org.$/', '.org', $value);
    $value = \preg_replace('/\.us.$/', '.us', $value);

    // Remove any character that is NOT a letter, number, or common punctuation.
    $value = \preg_replace("/[^(\x20-\x7E)]*/", '', $value);

    // The regex below has three sub-matches, the three sections in ().
    // What the regex found to match each sub-match, is returned in $matches[0],
    // $matches[1], etc. The middle sub-match should match the email address.
    // The two sub-matches on the end are (.*) which should match anything.  However, by
    // default the regex makes the sub-match "greedy" in that it expands what it can match
    // as far as possible.  The added question mark to make "(.*?)" makes the two
    // sub-matches on the ends be non-greedy.  The email address sub-match in the middle is
    // greedy and expands. If you don't do that, you get exactly one character before the @
    // in the email, because the first regex sucked up all the text possible.
    // The {2,63}  Each part of a domain name can be no longer than 63 characters.
    // Note this regex presumes the string as already been made all lower case.
    if (\preg_match('/(.*?)([a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63})(.*?)/', $value, $matches)) {
        $value = $matches[2];
    }

    $value = \filter_var($value, FILTER_SANITIZE_EMAIL);

    if (!\filter_var($value, FILTER_VALIDATE_EMAIL)) {
        Messages\set_reply(401, ['Please enter your best email address.']);

        return false;
    }

    if (\mb_strlen($value) > 100) {
        Messages\set_reply(401, ['Please enter a shorter email address.']);

        return false;
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
