<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks first, last, and middle names.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the name
 * @param string $value   The name text to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_name($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // use mb_strlen()
    if (\mb_strlen($value) < $options['range']['min']) {
        Messages\set_reply(400, ["Please enter a [{$name}] that is " .
                                 'at least [' . $options['range']['min'] . '] characters long.']);

        return false;
    }

    if (\mb_strlen($value) > $options['range']['max']) {
        Messages\set_reply(400, ["Please enter a [{$name}] that is " .
                                 'less than [' . $options['range']['max'] . '] characters long.']);

        return false;
    }

    // Regex for names https://andrewwoods.net/blog/2018/name-validation-regex/
    if (!\preg_match(
        '/^[A-Za-z\\x{00C0}-\\x{00FF}][A-Za-z\\x{00C0}-\\x{00FF}\\\'\\-]+' .
                     '([\\ A-Za-z\\x{00C0}-\\x{00FF}][A-Za-z\\x{00C0}-\\x{00FF}\\\'\\-]+)*/u',
        $value,
        $matches
    )) {
        Messages\set_reply(400, ["Please set {$name} to ??? ."]);

        return false;
    }
    $value = $matches[0];

    // If the name is in all capitals or all lowercase
    if (\mb_strtoupper($value, 'utf-8') === $value
        || \mb_strtolower($value, 'utf-8') === $value) {
        // automatically uppercase the first letter
        $value = \ucfirst(\mb_strtolower($value));
    }

    // todo:: remove duplicate hyphens, apostrophes, spaces
    // Replace 3 consecutive characters or more with with 2.
    // Examples: 999999 becomes 999 and eeeaaaaa becomes eeeaaa
    // $value = \preg_replace('#((\w)\\3{1})\\3+#', "$1", $value);

    Check\set_key_value($name, $value, 'string');

    return true;
}
