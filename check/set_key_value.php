<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Messages;

/**
 * Sets a key value pair with the value type
 * Get the sanitized, edited, and validated value
 * from Check\get_key_value($name);.
 *
 * Specify the type to convert variable to. The possible types are:
 * boolean, bool, integer, float, double, string, array, object, null
 *
 * @param string $key   The index key or name of the variable
 * @param mixed  $value The value after it has been validated
 * @param string $type  The data type of the variable value
 *
 * @return array $GLOBALS['only_set_in_check_set_key_value']
 */
function set_key_value(string $key, mixed $value, string $type): array
{
    if ($key === '') {
        Messages\respond(501, ['The key provided to set_key_value() is not set yet.']);
    }

    $types = [
        'array',
        'bool',
        'boolean',
        'double',
        'float',
        'int',
        'integer',
        'null',
        'object',
        'string'
    ];

    if (!\in_array($type, $types, true)) {
        Messages\respond(501, ['The type of value is not set correctly.']);
    }

    \settype($value, $type);

    if (!\array_key_exists('only_set_in_check_set_key_value', $GLOBALS)) {
        $GLOBALS['only_set_in_check_set_key_value'] = [];
    }

    $GLOBALS['only_set_in_check_set_key_value'][$key] = $value;

    return [$key => $value];
}
