<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Loops the $input array, gets the configuration to check the value
 * from Check\configure_variables(), sanitizes, and validates values
 * using the check functions defined below.
 * Each check function stores valid values in Check\set_key_value().
 * That way, get_key_value() always provides good data that is safe.
 *
 * @param null|array $inputs $_REQUESTS, $_COOKIE, $_SESSION, array
 *
 * @return array $inputs
 */
function set_inputs($inputs): array
{
    $set = false;
    $valid = [];

    $variables = Check\get_key_value('variables');

    if (!$inputs) {
        return [];
    }

    foreach ($inputs as $key => $value) {
        // Skip inputs that are NOT preconfigured
        if (!\array_key_exists($key, $variables)) {
            continue;
        }

        if (!\array_key_exists('check', $variables[$key])) {
            Messages\respond(501, ["The check option has not been configured for [{$key}] yet."]);
        }

        // Get the preconfigured options settings for the key
        // check_variable_options() checks all of the options
        $options = $variables[$key];

        if (\is_string($value)) {
            // Remove the encoding for the regex and other checks
            $value = \htmlspecialchars_decode($value, ENT_HTML5);
        }

        if ($variables[$key]['check'] === 'array') {
            $set = Check\check_array($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'boolean') {
            $set = Check\check_boolean($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'datetime') {
            $set = Check\check_datetime($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'email') {
            $set = Check\check_email($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'enum') {
            $set = Check\check_enum($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'float') {
            $set = Check\check_float($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'integer') {
            $set = Check\check_integer($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'name') {
            $set = Check\check_name($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'password') {
            $set = Check\check_password($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'phone') {
            $set = Check\check_phone($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'regex') {
            $set = Check\check_regex($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'string') {
            $set = Check\check_string($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'token') {
            $set = Check\check_token($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'username') {
            $set = Check\check_username($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'url') {
            $set = Check\check_url($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'uuid') {
            $set = Check\check_uuid($key, $value, $options);
        } elseif ($variables[$key]['check'] === 'postal_code') {
            $set = Check\check_postal_code($key, $value, $options);
        }

        // Check\set_key_value() only sets valid data
        if ($set === true) {
            // Add the sanitized and validated values
            $valid[$key] = Check\get_key_value($key);
        }
    }

    return $valid;
}
