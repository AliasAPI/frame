<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure the variable value is a float within a range.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 * https://github.com/auraphp/Aura.Filter/blob/2.x/src/Rule/Sanitize/Double.php
 *
 * @param string $name    The variable name of the float
 * @param string $value   The float value to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_float($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    // Remember if the number is negative
    $is_negative = \preg_match('/^-/', $value);

    // Remove all non-numeric characters except the minus sign
    $value = \preg_replace('/[^0-9\.-]/', '', $value);

    // Reduce repeated decimals and minuses
    $value = \preg_replace('/[\.-]{2,}/', '.', $value);

    // Remove all decimals without a digit or minus next to them
    $value = \preg_replace('/([^0-9-]\.[^0-9])/', '', $value);

    // Remove all trailing decimals and minuses
    $value = \rtrim($value, '.-');

    // Remove negative signs between numbers
    $value = \str_replace('-', '', $value);

    if ($is_negative) {
        $value = '-' . $value;
    }

    // Remove all decimals but the first
    $pos = \mb_strpos($value, '.');

    $value = \str_replace('.', '', $value);

    if ($pos !== false) {
        $value = \mb_substr($value, 0, $pos)
                . '.'
                . \mb_substr($value, $pos);
    }

    if (!\filter_var($value, FILTER_VALIDATE_FLOAT)) {
        Messages\set_reply(400, ["The [{$value}] value is not a float."]);

        return false;
    }

    if (!\array_key_exists('min', $options)
        || !\filter_var($options['min'], FILTER_VALIDATE_FLOAT)) {
        Messages\set_reply(501, ['The options min value is not a float.']);

        return false;
    }

    if (!\array_key_exists('max', $options)
        || !\filter_var($options['max'], FILTER_VALIDATE_FLOAT)) {
        Messages\set_reply(501, ['The options max value is not a float.']);

        return false;
    }

    if ($value < $options['min']) {
        Messages\set_reply(400, ["The [{$value}] is below the [" . $options['min'] . '] mimimum.']);

        return false;
    }

    if ($value > $options['max']) {
        Messages\set_reply(400, ["The [{$value}] is above the [" . $options['max'] . '] maximum.']);

        return false;
    }

    Check\set_key_value($name, $value, 'float');

    return true;
}
