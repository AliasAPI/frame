<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Sanitizes, (re)formats, and validates a username.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the username
 * @param string $value   The text string to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_username($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    $value = \filter_var($value, FILTER_SANITIZE_STRING);

    // Remove all spaces from the username
    $value = \preg_replace('/\s+/', '', $value);

    // Replace all non-alphanumeric characters with an single underscore
    $value = \preg_replace('/[^a-zA-Z0-9]+/', '_', $value);

    // Remove unwanted leading (numeric) characters
    $value = \ltrim($value, $characters = " \n\r\t\v\0\1\2\3\4\5\6\7\\8\\9\\_");

    $value = \rtrim($value, $characters = " \n\r\t\v\\_");

    // Replace 3 consecutive characters or more with with 2
    // Examples: 99999 becomes 99 and eeeaaaaa becomes eeaa
    $value = \preg_replace('#((\w)\\2{1})\\2+#', '$1', $value);

    // Reduce the username to 20 characters if it is longer
    $value = (\mb_strlen($value) > 20) ? \mb_substr($value, 0, 20) : $value;

    // Uppercase the first letter
    $first_character = \mb_strtoupper(\mb_substr($value, 0, 1));

    $value = $first_character . \mb_substr($value, 1);

    if (\mb_strlen($value) < 4) {
        Messages\set_reply(401, ["Please use a {$name} with at least 4 characters."]);

        return false;
    }

    if (!\preg_match('/^([A-Za-z0-9_]+)$/', $value, $matches)) {
        Messages\set_reply(401, ["Please use a {$name} with only letters, numbers, and underscores."]);

        return false;
    }
    $value = $matches[0];

    Check\set_key_value($name, $value, 'string');

    return true;
}
