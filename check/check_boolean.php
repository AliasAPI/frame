<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks to make sure all the keys and values are present based on type.
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 *
 * @param string $name    The variable name of the boolean
 * @param mixed  $value   The mixed value to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is good
 */
function check_boolean($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    $bool = \filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

    // If the $value is not boolean, filter_var() returns NULL
    if ($bool === null) {
        Messages\set_reply(400, ["Please set [{$name}] to true or false."]);

        return false;
    }

    Check\set_key_value($name, $value, 'boolean');

    return true;
}
