<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check;
use AliasAPI\Messages;

/**
 * Checks the token to make sure it is formatted correctly and has not expired.
 *
 * CSRF token format: random_crypto_number . sha1(identifier) . expiration_time
 *
 * @param string $name    The variable name of the token
 * @param string $value   The token to to be checked
 * @param array  $options The custom check configuration
 *
 * @return bool true|false  Returns true if the value is OK
 */
function check_token($name, $value, $options = []): bool
{
    $options = Check\set_variable_options($name, $options);

    if ($options['nullable'] === true
        && $value === '') {
        Check\set_key_value($name, $value, 'string');

        return true;
    }

    $value = \filter_var($value, FILTER_SANITIZE_STRING);

    // If passed as an option, check the local saved token against the input token
    if (\array_key_exists($name, $options)
        // which checks the id
        && \mb_strlen($options[$name]) > 1
        // Use hash_equals() to prevent timing attacks
        // https://paragonie.com/blog/2015/11/preventing-timing-attacks-on-string-comparison-with-double-hmac-strategy
        && !\hash_equals($value, $options[$name])) {
        Messages\set_reply(403, ["Please retry; The [{$name}] token is not valid."]);

        return false;
    }

    if (\count(\explode('-', $value)) !== 3) {
        Messages\set_reply(500, ["The [{$name}] token does not have a valid format."]);

        return false;
    }

    [$random, $id, $token_expires] = \explode('-', $value);

    if (\time() >= $token_expires) {
        Messages\set_reply(403, ["Please retry; The [{$name}] token has expired."]);

        return false;
    }

    Check\set_key_value($name, $value, 'string');

    return true;
}
