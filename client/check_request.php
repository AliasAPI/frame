<?php

declare(strict_types=1);

namespace AliasAPI\Client;

use AliasAPI\Messages;

/**
 * Checks if the request has been set up correctly.
 *
 * @param array $request The request to be checked
 *
 * @return void returns a response 501 message if there is an error
 */
function check_request($request): void
{
    if (!isset($request['action'])
        || empty($request['action'])) {
        Messages\respond(501, ['The request action has not been set correctly.']);
    }

    if (!isset($request['pair']['client'])
        || empty($request['pair']['client'])) {
        Messages\respond(501, ['The pair client has not been set correctly.']);
    }

    if (!isset($request['pair']['server'])
        || empty($request['pair']['server'])) {
        Messages\respond(501, ['The pair server has not been set correctly.']);
    }
}
