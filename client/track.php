<?php

declare(strict_types=1);

namespace AliasAPI\Client;

use AliasAPI\Alias;
use AliasAPI\Autoload;
use AliasAPI\Client;
use AliasAPI\CrudJson;
use AliasAPI\CrudPair;
use AliasAPI\Crypto;
use AliasAPI\Debug;
use AliasAPI\Messages;
use AliasAPI\Server;

/**
 * Receives a request from the Client and returns a response in a procedure.
 */
function track(array $request): array
{
    require_once \dirname(__DIR__) . '/server/bootstrap.php';

    $train = Server\bootstrap();

    Autoload\require_autoload_files();

    $train = Autoload\track($train);

    $train = Debug\configure_debug($train);

    $train = CrudJson\define_writablepath($train, false);

    $settings = CrudJson\define_directory('CONFIGPATH', 'config', $train);

    Client\check_request($request);

    $service = Crypto\setup_services($request);

    $updated = CrudJson\setup_config_file($request, '.services.json', $service);

    $services = Client\read_services_json_file();

    $alias_setup = Crypto\setup_alias($request, $services, 'client');

    $updated = CrudJson\setup_config_file($request, '.alias.json', $alias_setup);

    $alias_attributes = Alias\track($request['pair']['client'], []);

    $request = CrudPair\track($request, $alias_attributes, $services);

    $tag = Messages\set_request_tag($request, false);

    $method = Messages\set_method();

    $server_url = Messages\set_server_url($services[$request['pair']['server']]['url']);

    $pair_parameters = CrudPair\get_pair_parameters();

    $request = Crypto\lock_body($request, $alias_attributes, $pair_parameters);

    $body = Messages\set_body($request);

    $headers = Messages\set_headers([], $body);

    return $request;
}
