<?php

declare(strict_types=1);

namespace AliasAPI\Client;

use AliasAPI\CrudJson;
use AliasAPI\Messages;

// This is in client/
/**
 * Loads the services configs file.
 *
 * @return array returns the Load Configuration File
 */
function read_services_json_file(): array
{
    $services_file = [];

    if (!\defined('CONFIGPATH')) {
        Messages\respond(501, ['The CONFIGPATH constant is not defined.']);
    }

    $services_file = CrudJson\read_json_file(CONFIGPATH, '.services.json');

    if (\count($services_file) === 0) {
        Messages\respond(501, ['Could not read the services json file.']);
    }

    return $services_file;
}
