<?php

declare(strict_types=1);

namespace AliasAPI\Server;

/**
 * Set the initial settings.
 *
 * @param array $train
 *
 * @return array $train    The initial $settings to bootstrap the service
 */
function bootstrap($train = []): array
{
    \date_default_timezone_set('UTC');

    require_once \dirname(__DIR__, 1) . '/autoload/require_autoload_files.php';

    require_once \dirname(__DIR__, 1) . '/server/track.php';

    require_once \dirname(__DIR__, 1) . '/debug/say.php';

    return $train;
}
