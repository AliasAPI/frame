<?php

declare(strict_types=1);

namespace AliasAPI\Server;

use AliasAPI\Messages;

/**
 * Creates the pod.
 *
 * @param array $alias_attributes the alias attributes array
 * @param array $merged           the merged array
 * @param array $pair_parameters  the pair_parameters array
 *
 * @return array $train             returns the $train
 */
function depart(array $alias_attributes, array $merged, array $pair_parameters): array
{
    $train = $merged;

    $train['alias_attributes'] = $alias_attributes;
    $train['pair_parameters'] = $pair_parameters;
    $train['tag'] = Messages\get_request_tag();

    return $train;
}
