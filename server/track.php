<?php

declare(strict_types=1);

namespace AliasAPI\Server;

use AliasAPI\Alias;
use AliasAPI\Autoload;
use AliasAPI\Construe;
use AliasAPI\CrudJson;
use AliasAPI\CrudPair;
use AliasAPI\CrudTable;
use AliasAPI\Crypto;
use AliasAPI\Debug;
use AliasAPI\Messages;
use AliasAPI\Server;

/**
 * Receives a request from the Client and returns a response in a procedure.
 *
 * @return array $train
 */
function track(array $train): array
{
    Autoload\require_autoload_files();

    $train = Autoload\define_docroot($train);

    $train = Autoload\configure_autoload_paths($train);

    $train = Autoload\configure_file_load_types($train);

    $train = Autoload\configure_recursive_depth($train);

    $load_files = Autoload\glob_autoload_paths_files($train);

    $extra_files = Autoload\configure_extra_autoload_files($train);

    Autoload\autoload_specified_files($load_files, $extra_files);

    $train = Debug\configure_debug($train);

    $train = CrudJson\define_writablepath($train, true);

    $request = Messages\get_server_request();

    Messages\check_server_request($request);

    $credentials = Messages\set_request_credentials($request);

    $tag = Messages\set_request_tag($request, false);

    $tag_file = CrudJson\read_json_file('', $tag);

    $train = Construe\set_train($train, $request, $tag_file);

    $train = CrudJson\define_directory('CONFIGPATH', 'config', $train);

    $alias_config = Crypto\setup_alias($train, [], 'server');

    $created = CrudJson\setup_config_file($train, '.alias.json', $alias_config);

    $alias = Alias\find_alias($train, '');

    $alias_configs_file = Alias\configure_alias_configs_file();

    $alias_file = Alias\read_alias_json_file($alias_configs_file);

    $alias_configurations = Alias\decode_alias_configurations($alias_file);

    Alias\set_alias_configs_global($alias_configurations);

    Alias\check_alias_api_user($alias, $alias_configurations);

    $alias_attributes = Alias\set_alias_attributes($alias);

    Alias\check_alias_api_pass($credentials, $alias_attributes);

    CrudTable\set_pdo_options($alias_attributes);

    CrudTable\set_credentials($alias_attributes);

    CrudTable\set_connection($alias_attributes);

    CrudTable\create_database($alias_attributes);

    $train = CrudPair\set_pre_pair($train, $alias_attributes, []);

    CrudPair\set_pair_auth($train, []);

    CrudPair\delete_pair_file($train);

    CrudPair\create_pair_file($train);

    $pair_file = CrudPair\read_pair_file($train);

    $pair_parameters = CrudPair\set_pair_parameters($train, $pair_file);

    $body = Crypto\unlock_body($train, $alias_attributes, $pair_parameters);

    $merged = Construe\merge_inputs($train, $body);

    Alias\check_alias_api_auth($merged, $alias_attributes);

    return Server\depart($alias_attributes, $merged, $pair_parameters);
}
