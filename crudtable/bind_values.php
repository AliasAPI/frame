<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Bind the values for the query.
 *
 * @param object $statement  the query on which link the values
 * @param array  $key_pairs  associative array containing the values to bind
 * @param array  $type_array associative array with the desired value for its corresponding key in $array
 *
 * @return void returns the values for the query
 */
function bind_values(object $statement, array $key_pairs, array $type_array = []): void
{
    $type = 0;

    if (!($statement instanceof \PDOStatement)) {
        Messages\respond(501, ['The bind_sql statement has not been prepared.']);
    }

    if (\count($key_pairs) > 0) {
        foreach ($key_pairs as $key => $value) {
            $key = CrudTable\sanitize_name($key);

            if (\count($type_array) > 0) {
                $statement->bindValue(":{$key}", $value, $type_array[$key]);
            } else {
                if (\is_string($value)) {
                    $type = \PDO::PARAM_STR;
                } elseif (\is_int($value)) {
                    $type = \PDO::PARAM_INT;
                } elseif ($value === null) {
                    $type = \PDO::PARAM_NULL;
                } elseif (\is_bool($value)) {
                    $type = \PDO::PARAM_BOOL;
                } else {
                    Messages\respond(501, ['Unknown parameter type.']);
                }

                $statement->bindValue(":{$key}", $value, $type);
            }
        }
    }
}
