<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use PDO;

/**
 * Sets the PDO options.
 *
 * @return array Returns a 500 error. Otherwise return the database connection.
 */
function set_pdo_options(array $alias_attributes): array
{
    if (\array_key_exists('side', $alias_attributes)
        && $alias_attributes['side'] === 'client') {
        return [];
    }

    $GLOBALS['only_set_in_crudtable_set_pdo_options'] = [
        // Important! Use actual prepared statements
        // (Default: emulate prepared statements)
        \PDO::ATTR_EMULATE_PREPARES => false,
        // Throw exceptions on errors (default: stay silent)
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        // Fetch associative arrays (default: mixed arrays)
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
    ];

    return $GLOBALS['only_set_in_crudtable_set_pdo_options'];
}
