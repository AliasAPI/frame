<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Gets the columns for a table using any PDO driver.
 * Note: The LIMIT 0 is added for improved performance.
 *
 * @param string $table The name of the table
 *
 * @return array $columns  Returns the column names of the table
 */
function get_table_columns($table): array
{
    $columns = [];

    try {
        $connection = CrudTable\get_connection();

        $sql = "SELECT * FROM `{$table}`  LIMIT 0";

        $result = $connection->query($sql);

        for ($i = 0; $i < $result->columnCount(); ++$i) {
            $column = $result->getColumnMeta($i);
            $columns[] = $column['name'];
        }
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getmessage()]);
    }

    return $columns;
}
