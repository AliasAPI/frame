<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Messages;

/**
 * Gets the connection to the database.
 *
 * @return object Returns a 501 error if there is no connection set. Otherwise return the database connection.
 */
function get_connection(): ?object
{
    if (!isset($GLOBALS['only_set_in_crudtable_set_connection'])
        || empty($GLOBALS['only_set_in_crudtable_set_connection'])) {
        Messages\respond(501, ['The database connection is not set.']);
    }

    return $GLOBALS['only_set_in_crudtable_set_connection'];
}
