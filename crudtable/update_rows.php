<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Updates the row from the table.
 *
 * @param string $table        the table name
 * @param array  $update_pairs The update key value pairs to update
 * @param array  $where_pairs  The where key value pairs to select rows
 *
 * @return int $row_count The number of affected rows in the database
 */
function update_rows(string $table, array $update_pairs, array $where_pairs): int
{
    $updates = '';
    $i = 0;
    $wheres = '';
    $row_count = 0;

    try {
        foreach ($update_pairs as $key => $value) {
            $updates .= "{$key} = :{$key}, ";
        }

        $updates = \trim($updates, ', ');

        foreach ($where_pairs as $key => $value) {
            $wheres .= " AND {$key} = :{$key}";
        }

        $wheres = \trim($wheres, 'AND ');

        $bind_sql = "UPDATE `{$table}` " .
                    "SET {$updates} " .
                    "WHERE {$wheres}";

        $statement = CrudTable\query($bind_sql, $update_pairs, $where_pairs);

        $row_count = $statement->rowCount();
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getMessage()]);
    }

    return $row_count;
}
