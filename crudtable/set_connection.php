<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Sets the connection of the database.
 *
 * @return object create a database connection or return a 501 error
 */
function set_connection(array $alias_attributes): null|object
{
    if (\array_key_exists('side', $alias_attributes)
        && $alias_attributes['side'] === 'client') {
        return null;
    }

    $credentials = CrudTable\get_credentials();

    $options = CrudTable\get_pdo_options();

    try {
        $GLOBALS['only_set_in_crudtable_set_connection'] = new \PDO(
            $credentials['dsn'],
            $credentials['username'],
            $credentials['password'],
            $options
        );
    } catch (\PDOException $ex) {
        Messages\respond(501, [$ex->getMessage()]);
    }

    if (!\array_key_exists('only_set_in_crudtable_set_connection', $GLOBALS)) {
        Messages\respond(501, ['The PDO connection could not be created.']);
    }

    return $GLOBALS['only_set_in_crudtable_set_connection'];
}
