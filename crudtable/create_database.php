<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Creates the database if the request is setup services.
 */
function create_database(array $alias_attributes): void
{
    if (\array_key_exists('side', $alias_attributes)
        && $alias_attributes['side'] === 'client') {
        return;
    }

    try {
        $cred = CrudTable\get_credentials();

        $conn = CrudTable\get_connection();

        $exists = $conn->query('SHOW DATABASES')->fetchAll(\PDO::FETCH_ASSOC);

        if (!in_array($cred['database'], $exists, true)) {
            $sql = 'CREATE DATABASE IF NOT EXISTS ' . $cred['database'] . '
                    CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';

            $conn->exec($sql);
        }

        $conn->exec('USE ' . $cred['database']);
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getmessage()]);
    }
}
