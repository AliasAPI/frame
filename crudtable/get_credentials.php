<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Messages;

/**
 * Gets the credentials of the database.
 *
 * @return array Returns a 501 error if there is no connection set. Otherwise return the database credentials.
 */
function get_credentials(): ?array
{
    if (!isset($GLOBALS['only_set_in_crudtable_set_credentials'])
        || empty($GLOBALS['only_set_in_crudtable_set_credentials'])) {
        Messages\respond(501, ['The database credentials are not set.']);
    }

    return $GLOBALS['only_set_in_crudtable_set_credentials'];
}
