<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Deletes the rows from the table.
 *
 * @param string $table       the table name
 * @param array  $where_pairs the where pairs
 * @param int    $limit       how many rows to show
 *
 * @return mixed returns a 501 error response if there is an error in the query. Otherwise return the rowcount.
 */
function delete_rows(string $table, array $where_pairs, int $limit = 0): mixed
{
    $wheres = '';

    try {
        foreach ($where_pairs as $key => $value) {
            $wheres .= " AND {$key} = :{$key}";
        }

        $wheres = \trim($wheres, 'AND ');

        // if limit is a number use a limit on the query
        if ($limit > 0) {
            $limiter = "LIMIT {$limit}";
        } else {
            $limiter = '';
        }

        $bind_sql = "DELETE FROM `{$table}` " .
                    "WHERE {$wheres} " .
                    "{$limiter}";

        $statement = CrudTable\query($bind_sql, [], $where_pairs);

        return $statement->rowCount();
    } catch (\Exception $ex) {
        Messages\respond(500, [$ex->getMessage()]);
    }
}
