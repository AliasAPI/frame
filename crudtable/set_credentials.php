<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Messages;

/**
 * Sets the credentials for the database connection.
 *
 * @param array $alias_attributes The the configuration array with database credentials
 *
 * @return array returns a 501 error if there is no connection set
 */
function set_credentials($alias_attributes): array
{
    $GLOBALS['only_set_in_crudtable_set_credentials'] = [];

    if (\array_key_exists('side', $alias_attributes)
        && $alias_attributes['side'] === 'client') {
        return [];
    }

    $database_creds = $alias_attributes['database_config'] ?? [];

    if (!isset($database_creds['dsn'])
        || empty($database_creds['dsn'])) {
        Messages\respond(501, ['The database dsn is not configured.']);
    }

    if (!isset($database_creds['database'])
        || empty($database_creds['database'])) {
        Messages\respond(501, ['The database name is not configured.']);
    }

    if (!isset($database_creds['username'])
        || empty($database_creds['username'])) {
        Messages\respond(501, ['The database username is not configured.']);
    }

    if (!isset($database_creds['password'])) {
        Messages\respond(501, ['The database password is not configured.']);
    }

    $GLOBALS['only_set_in_crudtable_set_credentials']['dsn'] = $database_creds['dsn'];
    $GLOBALS['only_set_in_crudtable_set_credentials']['database'] = $database_creds['database'];
    $GLOBALS['only_set_in_crudtable_set_credentials']['username'] = $database_creds['username'];
    $GLOBALS['only_set_in_crudtable_set_credentials']['password'] = $database_creds['password'];

    return $GLOBALS['only_set_in_crudtable_set_credentials'];
}
