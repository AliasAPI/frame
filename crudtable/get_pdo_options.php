<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\Messages;

/**
 * Gets the pdo options.
 *
 * @return array Returns a 501 error if there are no PDO options set. Otherwise return the PDO Options.
 */
function get_pdo_options(): array
{
    if (!\array_key_exists('only_set_in_crudtable_set_pdo_options', $GLOBALS)
        || empty($GLOBALS['only_set_in_crudtable_set_pdo_options'])) {
        Messages\respond(501, ['The PDO options are not set.']);
    }

    return $GLOBALS['only_set_in_crudtable_set_pdo_options'];
}
