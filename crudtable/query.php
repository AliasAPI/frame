<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Executes a query.
 *
 * @param string $bind_sql     the query line
 * @param array  $update_pairs the update pairs
 * @param array  $where_pairs  the where pairs
 *
 * @return mixed Returns a 501 error if there is a query error. Otherwise execute the query.
 */
function query(string $bind_sql, array $update_pairs = [], array $where_pairs = []): mixed
{
    $statement = '';

    try {
        $connection = CrudTable\get_connection();

        // If creating a database or table, just run the query
        if (empty($where_pairs)) {
            return $connection->exec($bind_sql);
        }

        $statement = $connection->prepare($bind_sql);

        CrudTable\bind_values($statement, $where_pairs);

        CrudTable\bind_values($statement, $update_pairs);

        $statement->execute();
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getmessage()]);
    }

    return $statement;
}
