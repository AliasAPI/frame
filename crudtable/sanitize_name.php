<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

/**
 * Makes the name lowercase and removes special characters & allow underscores.
 *
 * @param string $name the name to be sanitized
 *
 * @return string returns the sanitized name
 */
function sanitize_name(string $name): string
{
    // The name should be lowercase
    $name = \mb_strtolower($name);

    // Remove special characters & allow underscores
    return \preg_replace('/[^a-z0-9_]+/', '', $name);
}
