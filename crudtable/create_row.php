<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Creates a new query row.
 *
 * @param string $table       the table name
 * @param array  $where_pairs the where pairs
 *
 * @return int $last_id       Or it returns a 501 error
 */
function create_row(string $table, array $where_pairs): int
{
    $last_id = 0;

    try {
        $bind_sql = "INSERT IGNORE INTO `{$table}` " .
                    // Format the columns names separated by commas
                    '(' . \implode(',', \array_keys($where_pairs)) . ') ' .
                    // Format the values with the colons in bind sql
                    'VALUES (:' . \implode(', :', \array_keys($where_pairs)) . ') ' .
                    'LIMIT 1';

        $statement = CrudTable\query($bind_sql, [], $where_pairs);

        $connection = CrudTable\get_connection();

        $last_id = (int) $connection->lastInsertId();
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getMessage()]);
    }

    return $last_id;
}
