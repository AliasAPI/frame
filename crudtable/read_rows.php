<?php

declare(strict_types=1);

namespace AliasAPI\CrudTable;

use AliasAPI\CrudTable;
use AliasAPI\Messages;

/**
 * Selects rows from a table.
 *
 * @param string $table       the table name
 * @param array  $where_pairs the where pairs
 * @param int    $limit       the number of rows to be shown
 *
 * @return array Returns a 501 error if there is an error. Otherwise return the rows.
 */
function read_rows(string $table, array $where_pairs, int $limit = 10): array
{
    $rows = [];
    $wheres = '';

    if (count($where_pairs) === 0) {
        Messages\respond(501, ['The where_pairs input is empty.']);
    }

    try {
        foreach ($where_pairs as $key => $value) {
            $wheres .= " AND `{$key}` = :{$key}";
        }

        $wheres = \trim($wheres, 'AND ');

        $bind_sql = "SELECT * FROM `{$table}` " .
                    "WHERE {$wheres} " .
                    "LIMIT {$limit}";

        $statement = CrudTable\query($bind_sql, [], $where_pairs);

        $rows = (array) $statement->fetchAll();

        // If there is just one row, return it without the [0] index
        // to simplify the subsequent code
        if ($limit === 1
            && isset($rows[0])) {
            return $rows[0];
        }
    } catch (\PDOException $ex) {
        Messages\respond(500, [$ex->getMessage()]);
    }

    return $rows;
}
