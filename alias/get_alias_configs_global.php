<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Gets the alias global configuration.
 *
 * @return array Returns the global configuration if TRUE, otherwise it will return a response 501
 */
function get_alias_configs_global(): array
{
    if (!isset($GLOBALS['only_set_in_set_alias_configs_global'])
        || empty($GLOBALS['only_set_in_set_alias_configs_global'])) {
        Messages\respond(501, ['The alias configs global is not set.']);
    }

    return $GLOBALS['only_set_in_set_alias_configs_global'];
}
