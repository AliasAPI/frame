<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Checks if the alias is found in the alias_configs_file.
 *
 * @param string $alias                The name of Alias
 * @param array  $alias_configurations alias_configs_file array
 *
 * @return void Returns response 400 if alias is not found
 */
function check_alias_api_user(string $alias, array $alias_configurations): void
{
    if ($alias === '') {
        Messages\respond(400, ['The alias is not set in the request or tag file.']);
    } elseif (!\array_key_exists($alias, $alias_configurations)) {
        Messages\respond(400, ['The alias is not found in the alias_configs_file.']);
    }
}
