<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Checks if alias is authorized to perform the action.
 *
 * @param array $train            the actions to be performed
 * @param array $alias_attributes attribute of action
 *
 * @return void  return Response 403 if alias is not authorized or not set
 */
function check_alias_api_auth(array $train, array $alias_attributes): void
{
    if (\array_key_exists('method', $train)
        && $train['method'] !== 'GET'
        && !\array_key_exists('action', $train)) {
        Messages\respond(403, ['The action is not set in the request or tag_file.']);
    }

    if (\array_key_exists('action', $train)
        // The authorization of action is only on the server side
        && !\in_array($train['action'], $alias_attributes['authorized_actions'], true)) {
        Messages\respond(403, ['The alias is not authorized to perform the action.']);
    }
}
