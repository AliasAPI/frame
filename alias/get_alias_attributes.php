<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

/**
 * Gets the attributes of the alias.
 *
 * @return array $alias_attributes
 */
function get_alias_attributes(): array
{
    if (!isset($GLOBALS['only_set_in_alias_set_alias_attributes'])) {
        return [];
    }

    return $GLOBALS['only_set_in_alias_set_alias_attributes'];
}
