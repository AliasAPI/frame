<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Check to make sure all of the alias attributes are configured.
 *
 * @param array $attributes specify the attribute to check
 *
 * @return void returns a response 501 depending on the specifed attribute
 */
function check_alias_attributes(array $attributes): void
{
    if (\count($attributes) === 0) {
        Messages\respond(501, ['The Alias has no attributes.']);
    }

    if (!\array_key_exists('alias', $attributes)) {
        Messages\respond(501, ['The Alias has not been configured.']);
    }

    if (!\array_key_exists('api_pass', $attributes['api_keys'])) {
        Messages\respond(501, ['The Alias api_pass has not been configured.']);
    }

    if (!\array_key_exists('salt', $attributes['api_keys'])) {
        Messages\respond(501, ['The Alias salt has not been configured.']);
    }

    if (!\array_key_exists('authorized_actions', $attributes)
        || \count($attributes['authorized_actions']) === 0) {
        Messages\respond(501, ['The Alias has no authorized API actions configured.']);
    }

    if (!\array_key_exists('side', $attributes)
        || ($attributes['side'] !== 'client'
            && $attributes['side'] !== 'server')) {
        Messages\respond(501, ['The Alias side has not been configured correctly.']);
    } elseif ($attributes['side'] === 'client') {
        // The client does not need crypto_config, database_config, debug_config, email_config
        return;
    }

    if (!\array_key_exists('crypto_config', $attributes)) {
        Messages\respond(501, ['The Alias crypto_config has not been configured correctly.']);
    }

    if (!\array_key_exists('sign', $attributes['crypto_config'])
        || ($attributes['crypto_config']['sign'] !== true
            && $attributes['crypto_config']['sign'] !== false)) {
        Messages\respond(501, ['The Alias crypto_config sign has not been configured correctly.']);
    }

    if (!\array_key_exists('encrypt', $attributes['crypto_config'])
        || ($attributes['crypto_config']['encrypt'] !== true
            && $attributes['crypto_config']['encrypt'] !== false)) {
        Messages\respond(501, ['The Alias crypto_config encrypt has not been configured correctly.']);
    }

    if (!\array_key_exists('debug_config', $attributes)) {
        Messages\respond(501, ['The Alias debug_config has not been configured correctly.']);
    }

    if (!\array_key_exists('detail_level', $attributes['debug_config'])
        || !\is_int($attributes['debug_config']['detail_level'])) {
        Messages\respond(501, ['The Alias debug_config detail_level has not been configured correctly.']);
    }

    if (!\array_key_exists('environment', $attributes['debug_config'])
        || ($attributes['debug_config']['environment'] !== 'production'
            && $attributes['debug_config']['environment'] !== 'development')) {
        Messages\respond(501, ['The Alias debug_config environment has not been configured correctly.']);
    }

    if (!\array_key_exists('dsn', $attributes['database_config'])
        || $attributes['database_config']['dsn'] === '') {
        Messages\respond(501, ['The database_config dsn has not been configured correctly.']);
    }

    if (!\array_key_exists('username', $attributes['database_config'])
        || $attributes['database_config']['username'] === '') {
        Messages\respond(501, ['Manually add the database_config username in the .alias.json.']);
    }

    if (!\array_key_exists('password', $attributes['database_config'])) {
        Messages\respond(501, ['The database_config password has not been configured correctly.']);
    }

    if (!\array_key_exists('service_uuid', $attributes)
        || $attributes['service_uuid'] === '') {
        Messages\respond(501, ['The service uuid has not been configured correctly.']);
    }
}
