<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Receives a request from the Client and returns a response in a procedure.
 *
 * @return string $alias_json file
 */
function read_alias_json_file(string $alias_json_configs_file): string
{
    if (!\file_exists($alias_json_configs_file)) {
        Messages\respond(501, ['The alias json configurations file does not exist.']);
    }

    $alias_json = \file_get_contents($alias_json_configs_file);

    if ($alias_json === false) {
        Messages\respond(501, ['Could not load the alias configurations file.']);
    }

    return $alias_json;
}
