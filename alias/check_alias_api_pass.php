<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Authorization to check if api_pass is incorrect.
 *
 * @param array $request_credentials the api_pass requested
 * @param array $alias_attributes    the api_pass attribute
 *
 * @return void returns response 401 if api_pass is incorrect
 */
function check_alias_api_pass(array $request_credentials, array $alias_attributes): void
{
    if (isset($request_credentials['api_pass'])
        // todo:: add the hash and prevent timing attacks (like with sessions/csrf/etc)
        && $request_credentials['api_pass'] !== $alias_attributes['api_keys']['api_pass']) {
        Messages\respond(401, ['The api_pass is incorrect.']);
    }
}
