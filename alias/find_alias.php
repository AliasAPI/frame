<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Checks if the alias is included in the request.
 * CAUTION: Do not accept the alias from $_GET requests.
 *
 * @param string $alias the alias name
 * @param array  $train the request
 *
 * @return string $alias    returns the alias name if TRUE, otherwise returns respose 400
 */
function find_alias(array $train, string $alias = ''): string
{
    if (\array_key_exists('pair', $train)
        && \array_key_exists('server', $train['pair'])
        && $train['pair']['server'] !== '') {
        $alias = $train['pair']['server'];
    } elseif (\array_key_exists('api_user', $train)
              && $train['api_user'] !== '') {
        $alias = $train['api_user'];
    } elseif ($alias !== '') {
        return $alias;
    }

    if ($alias === '') {
        Messages\respond(400, ['There is no alias found in the request.']);
    }

    return $alias;
}
