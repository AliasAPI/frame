<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Alias;

/**
 * Receives a request from the Client and returns a response in a procedure.
 */
function track(string $alias, array $train): array
{
    $alias = Alias\find_alias($train, $alias);

    $alias_json_configs_file = Alias\configure_alias_configs_file();

    $file = Alias\read_alias_json_file($alias_json_configs_file);

    $alias_configurations = Alias\decode_alias_configurations($file);

    Alias\set_alias_configs_global($alias_configurations);

    Alias\check_alias_api_user($alias, $alias_configurations);

    $alias_attributes = Alias\set_alias_attributes($alias);

    $alias_attributes = Alias\get_alias_attributes();

    Alias\check_alias_api_pass($train, $alias_attributes);

    Alias\check_alias_api_auth($train, $alias_attributes);

    return $alias_attributes;
}
