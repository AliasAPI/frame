<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Gets the DOCROOT of the config file.
 *
 * @return string returns the DOCROOT of the config file if true, else return response code 501
 */
function configure_alias_configs_file(): string
{
    if (!\defined('DOCROOT')) {
        Messages\respond(501, ['The DOCROOT constant is not defined.']);
    }

    return DOCROOT . 'config' . DIRECTORY_SEPARATOR . '.alias.json';
}
