<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Decodes the configurations and converts it to PHP variable.
 *
 * @param string $alias_json the JSON string converted into PHP variable
 *
 * @return array $alias_configurations
 */
function decode_alias_configurations(string $alias_json): array
{
    $alias_configurations = \json_decode($alias_json, true);

    if (empty($alias_configurations)) {
        Messages\respond(500, ['The alias configurations file cannot be decoded.']);
    }

    return $alias_configurations;
}
