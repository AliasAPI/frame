<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Alias;
use AliasAPI\Messages;

/**
 * Checks and sets the attributes of the alias.
 *
 * @param string $alias The alias name
 *
 * @return array returns the attributes of the alias if TRUE, otherwise return response 501
 */
function set_alias_attributes(string $alias): ?array
{
    if ($alias === '') {
        Messages\respond(501, ['The Alias is not provided.']);
    }

    $alias_configs = Alias\get_alias_configs_global();

    if (!\array_key_exists($alias, $alias_configs)) {
        Messages\respond(501, ['The Alias is not found in the alias_configs.']);
    }

    Alias\check_alias_attributes($alias_configs[$alias]);

    $GLOBALS['only_set_in_alias_set_alias_attributes'] = $alias_configs[$alias];

    return $GLOBALS['only_set_in_alias_set_alias_attributes'];
}
