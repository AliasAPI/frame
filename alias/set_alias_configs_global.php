<?php

declare(strict_types=1);

namespace AliasAPI\Alias;

use AliasAPI\Messages;

/**
 * Sets the global configurations for Alias.
 *
 * @param array $alias_configs the list of all configurations in the file
 *
 * @return array Returns the configurations as a global config if TRUE, otherwise it will return a response 501
 */
function set_alias_configs_global(array $alias_configs): array
{
    if (empty($alias_configs)) {
        Messages\respond(501, ['The alias_configurations array is empty.']);
    } else {
        $GLOBALS['only_set_in_set_alias_configs_global'] = $alias_configs;
    }

    return $GLOBALS['only_set_in_set_alias_configs_global'];
}
